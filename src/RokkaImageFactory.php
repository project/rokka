<?php

declare(strict_types = 1);

namespace Drupal\rokka;

use Drupal\Core\Image\Image;
use Drupal\Core\Image\ImageFactory;

/**
 * Alter the image factory service.
 */
class RokkaImageFactory extends ImageFactory {

  /**
   * {@inheritdoc}
   */
  public function get($source = NULL, $toolkit_id = NULL) {
    return new Image($this->toolkitManager->createInstance('rokka'), $source);
  }

  /**
   * {@inheritdoc}
   */
  public function getSupportedExtensions($toolkit_id = NULL) {
    $default = parent::getSupportedExtensions($toolkit_id);
    $rokka_specific = ['pdf', 'eps', 'webp', 'svg', 'tiff', 'heic'];
    return array_unique(array_merge($default, $rokka_specific), SORT_REGULAR);
  }

}
