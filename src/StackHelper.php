<?php

declare(strict_types=1);

namespace Drupal\rokka;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\image\Entity\ImageStyle;
use Drupal\rokka\Entity\RokkaStack;
use Drupal\rokka\StyleEffects\ImageEffectInterface;
use GuzzleHttp\Psr7\Uri;
use Rokka\Client\Core\Stack as RemoteStack;
use Rokka\Client\UriHelper;

/**
 * Image style and stack helper service.
 */
class StackHelper {

  use StringTranslationTrait;

  /**
   * Rokka service.
   *
   * @var \Drupal\rokka\RokkaServiceInterface
   */
  protected $rokkaService;

  /**
   * Rokka stack storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $storageRokkaStack;

  /**
   * Image style storage.
   *
   * @var \Drupal\image\ImageStyleStorage
   */
  protected $storageImageStyle;

  /**
   * Logger.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * StackHelper constructor.
   *
   * @param \Drupal\rokka\RokkaServiceInterface $rokkaService
   *   Rokka service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   Entity type manager.
   * @param \Drupal\Core\Logger\LoggerChannelInterface $logger
   *   Logger.
   */
  public function __construct(
    RokkaServiceInterface $rokkaService,
    EntityTypeManagerInterface $entityTypeManager,
    LoggerChannelInterface $logger,
  ) {
    $this->rokkaService = $rokkaService;
    $this->storageRokkaStack = $entityTypeManager->getStorage('rokka_stack');
    $this->storageImageStyle = $entityTypeManager->getStorage('image_style');
    $this->logger = $logger;
  }

  /**
   * Build stack operation collection.
   *
   * @param array $effects
   *   Effects.
   *
   * @return \Rokka\Client\Core\StackOperation[]
   *   Stack operations.
   */
  private static function buildStackOperationCollection(array $effects): ?array {
    if (empty($effects)) {
      $effects = [
        [
          'id' => 'noop',
          'data' => [],
          'weight' => 0,
        ],
      ];
    }

    $operations = [];
    $currentId = 0;

    // Sort effects by defined weight.
    usort($effects,
      static function ($a, $b) {
        return $a['weight'] - $b['weight'];
      }
    );

    foreach ($effects as $effect) {
      $options = self::buildStackOperation($effect);
      if (!empty($options)) {
        foreach ($options as $option) {
          $operations[$currentId++] = $option;
        }
      }
    }

    if (empty($operations)) {
      return NULL;
    }

    ksort($operations);
    return $operations;
  }

  /**
   * Build stack operation.
   *
   * @param array $effect
   *   Effects.
   *
   * @return \Rokka\Client\Core\StackOperation[]
   *   Operations.
   */
  public static function buildStackOperation(array $effect): array {
    $name = $effect['id'];
    $class_name = 'Drupal\rokka\StyleEffects\Effect' . self::convertToCamelCase(
        $name,
        TRUE
      );

    $result = [];
    if (
      class_exists($class_name) &&
      in_array(ImageEffectInterface::class, class_implements($class_name), TRUE)
    ) {
      /** @var \Drupal\rokka\StyleEffects\ImageEffectInterface $class_name */
      $result = $class_name::buildRokkaStackOperation(!empty($effect['data']) ? $effect['data'] : []);
    }
    else {
      // @todo Inject.
      \Drupal::logger('rokka')->error(
        'Can not convert effect "%effect" to Rokka.io StackOperation: "%class" Class missing!',
        [
          '%effect' => $name,
          '%class' => $class_name,
        ]
      );
    }

    return $result;
  }

  /**
   * Get effect stack.
   *
   * @param \Drupal\image\Entity\ImageStyle|null $image_style
   *   Image style.
   *
   * @return \Rokka\Client\Core\StackOperation[]
   *   Stack operations.
   */
  public static function getEffectStack(ImageStyle $image_style = NULL): ?array {
    $result = [];

    if ($image_style !== NULL) {
      $collection = self::buildStackOperationCollection($image_style->getEffects()->getConfiguration());
      if (!empty($collection)) {
        $result = $collection;
      }
    }

    return $result;
  }

  /**
   * Camel Case convert.
   *
   * @param string $subject
   *   Input string.
   * @param bool $class_case
   *   Class case.
   *
   * @return string
   *   Output string.
   */
  public static function convertToCamelCase(
    string $subject,
    $class_case = FALSE,
  ): string {
    // non-alpha and non-numeric characters become spaces.
    $subject = preg_replace('/[^a-z0-9]+/i', ' ', $subject);
    $subject = trim($subject);
    // Uppercase the first character of each word.
    $subject = ucwords($subject);
    $subject = str_replace(' ', '', $subject);
    if (!$class_case) {
      $subject = lcfirst($subject);
    }

    return $subject;
  }

  /**
   * Alter the image style form.
   *
   * @param array $form
   *   Form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state interface.
   */
  public function alterImageStyleForm(array &$form, FormStateInterface $form_state): void {
    /** @var \Drupal\image\Form\ImageStyleEditForm $form_object */
    $form_object = $form_state->getFormObject();
    // @todo Inject.
    $defaultConfig = \Drupal::config('rokka.settings');

    /** @var \Drupal\rokka\Entity\RokkaStack $stack_entity */
    $stack_entity = NULL;
    // Can be empty, if we create a new image style.
    if (!empty($form_object->getEntity()->get('name'))) {
      $stack_entity = $this->storageRokkaStack->load(
        $form_object->getEntity()->get('name')
      );
    }

    $form['rokka_stack_options'] = [
      '#type' => 'fieldset',
      '#title' => 'Rokka Stack settings',
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
      '#tree' => TRUE,
    ];

    if ($stack_entity instanceof RokkaStack) {
      $stack_options = $stack_entity->getStackOptions();
      $default_format = $stack_entity->getOutputFormat();
    }
    else {
      $stack_options = [];
      $default_format = $defaultConfig->get('output_format');
    }

    $form['rokka_stack_options']['output_format'] = [
      '#type' => 'select',
      '#title' => $this->t('Output format'),
      '#description' => $this->t('Defines the delivered image format.'),
      '#required' => TRUE,
      '#default_value' => $default_format,
      '#options' => [
        'jpg' => $this->t('JPG'),
        'png' => $this->t('PNG'),
        'gif' => $this->t('GIF'),
      ],
    ];

    $form['rokka_stack_options']['jpg_quality'] = [
      '#type' => 'number',
      '#min' => 0,
      '#max' => 100,
      '#title' => $this->t('JPG quality'),
      '#description' => $this->t(
        'Leave empty to inherit default. Example values:
            <ul>
            <li>0 (Determined by Rokka)</li>
            <li>1 (highest compression, lowest quality)</li>
            <li>100 (lowest compression, highest quality)</li></ul>'
      ),
      '#placeholder' => $defaultConfig->get('jpg_quality'),
    ];

    if (isset($stack_options['jpg_quality'])) {
      $form['rokka_stack_options']['jpg_quality']['#default_value'] = $stack_options['jpg_quality'];
    }

    $form['rokka_stack_options']['webp_quality'] = [
      '#type' => 'number',
      '#min' => 0,
      '#max' => 100,
      '#title' => $this->t('WEBP quality'),
      '#description' => $this->t(
        'Leave empty to inherit default. Example values:<ul>
            <li>0 (Determined by Rokka)</li>
            <li>1 (highest compression, lowest quality)</li>
            <li>100 (lowest compression, highest quality)</li></ul>'
      ),
      '#required' => FALSE,
      '#placeholder' => $defaultConfig->get('webp_quality'),
    ];

    if (isset($stack_options['webp_quality'])) {
      $form['rokka_stack_options']['webp_quality']['#default_value'] = $stack_options['webp_quality'];
    }

    $form['rokka_stack_options']['autoformat'] = [
      '#type' => 'radios',
      '#title' => $this->t('Autoformat'),
      '#description' => $this->t('Autoformat lets Rokka chose the best format supported by the browser.'),
      '#options' => [
        'none' => $this->t('Inherit default'),
        'true' => $this->t('Yes'),
        'false' => $this->t('No'),
      ],
      '#default_value' => 'none',
    ];

    if (isset($stack_options['autoformat']) && $stack_options['autoformat'] !== NULL) {
      $form['rokka_stack_options']['autoformat']['#default_value'] = $stack_options['autoformat'] ? 'true' : 'false';
    }
  }

  /**
   * Image style sync.
   *
   * @param \Drupal\image\Entity\ImageStyle $imageStyle
   *   Image style.
   */
  public function imageStyleSync(ImageStyle $imageStyle): void {
    /** @var \Drupal\rokka\Entity\RokkaStack $rokkaStack */
    $rokkaStack = $this->storageRokkaStack->load($imageStyle->id());
    if ($rokkaStack instanceof RokkaStack) {
      $this->logger->debug(
        'Updating existing Rokka stack @name',
        ['@name' => $imageStyle->getName()]
      );
      $formInput = [
        'output_format' => $rokkaStack->getOutputFormat(),
        'jpg_quality' => $rokkaStack->getStackOptions()['jpg_quality'],
        'webp_quality' => $rokkaStack->getStackOptions()['webp_quality'],
        'autoformat' => $rokkaStack->getStackOptions()['autoformat'],
      ];
    }
    else {
      $this->logger->debug(
        'Creating new Rokka stack @name',
        ['@name' => $imageStyle->getName()]
      );
      /** @var \Drupal\rokka\Entity\RokkaStack $rokkaStack */
      $rokkaStack = $this->storageRokkaStack
        ->create(['id' => $imageStyle->id()]);
      if (empty($imageStyle->get('rokka_stack_options'))) {
        // Sync triggered outside form with now input present.
        $formInput = [
          'output_format' => 'jpg',
          'jpg_quality' => NULL,
          'webp_quality' => NULL,
          'autoformat' => NULL,
        ];
        $this->logger->notice('Rokka stack created outside form, please check if your stack options are as desired.');
      }
    }

    // Form values take precedence.
    if (!empty($imageStyle->get('rokka_stack_options'))) {
      $formInput = $imageStyle->get('rokka_stack_options');
    }

    // Not saved on Rokka, only relevant for
    // RokkaStreamWrapper::getExternalUrl().
    $rokkaStack->setOutputFormat($formInput['output_format']);

    $defaultConfig = \Drupal::config('rokka.settings');

    // Remove values identical to default.
    if ($formInput['jpg_quality'] === $defaultConfig->get('jpg_quality')) {
      $formInput['jpg_quality'] = NULL;
    }
    if ($formInput['webp_quality'] === $defaultConfig->get('webp_quality')) {
      $formInput['webp_quality'] = NULL;
    }
    // Separate variable in configuration.
    unset($formInput['output_format']);

    // Workaround for checkboxes.
    if (in_array($formInput['autoformat'], ['none', NULL], TRUE)) {
      $formInput['autoformat'] = NULL;
    }
    else {
      $formInput['autoformat'] = $formInput['autoformat'] === 'true';
    }

    $rokkaStack->setStackOptions($formInput);

    $rokkaStack->enforceIsNew(FALSE);
    $rokkaStack->save();
  }

  /**
   * Image style preview.
   *
   * @param array $variables
   *   Variables.
   */
  public function imageStylePreview(array &$variables): void {
    $previewUrl = $variables['derivative']['url'];
    $previewUri = new Uri($previewUrl);
    $components = UriHelper::decomposeUri($previewUri);

    if ($components === NULL) {
      return;
    }

    $stackUri = $components->getStack();
    $imageStyle = $this->storageImageStyle->load($stackUri->getName());
    $stackEntity = $this->storageRokkaStack->load($stackUri->getName());

    $effectStack = self::getEffectStack($imageStyle);
    $stack = new RemoteStack(NULL, $components['stack'], $effectStack);
    if ($stackEntity instanceof RokkaStack) {
      $stack_options = $stackEntity->getStackOptions();
      if (is_array($stack_options)) {
        $stack->setStackOptions($stack_options);
      }
    }
    $dynamic_stack = $stack->getDynamicUriString();
    $components->setStack($dynamic_stack);
    $previewUrl = UriHelper::composeUri($components, $previewUri);
    $variables['derivative']['rendered']['#uri'] = (string) $previewUrl;
    $variables['derivative']['url'] = (string) $previewUrl;
    // We have to get the image from rokka to know its dimensions
    // that makes is a little bit slower, if anyone has a better idea..
    $image = @file_get_contents($previewUrl);
    if ($image !== FALSE) {
      $dimensions = $this->getImageDimensionsFromContent($image);
      $variables['derivative']['width'] = $dimensions['width'];
      $variables['derivative']['height'] = $dimensions['height'];
    }
    else {
      $variables['derivative']['width'] = 'unknown ';
      $variables['derivative']['height'] = 'unknown ';
    }
  }

  /**
   * Gets image dimensions via finfo.
   *
   * Finfo is a pretty fast way to get the dimensions of an image, unfortunately
   * it doesn't do that in an easy accessible way. We do it here from the
   * FILEINFO_RAW string and some regex magic.
   *
   * @param string $content
   *   Content.
   *
   * @return array
   *   Image dimensions.
   */
  private function getImageDimensionsFromContent(string $content): array {
    $answer = finfo_buffer(finfo_open(FILEINFO_RAW), $content);
    if (preg_match('/, *([0-9]{1,6}) *x *([0-9]{1,6})/', $answer, $matches)) {
      return ['width' => (int) $matches[1], 'height' => (int) $matches[2]];
    }

    return ['width' => NULL, 'height' => NULL];
  }

  /**
   * Delete stack on image style delete.
   *
   * @param \Drupal\image\Entity\ImageStyle $entity
   *   Image style.
   */
  public function deleteStackOnStyleDelete(ImageStyle $entity): void {
    \Drupal::configFactory()
      ->getEditable('rokka.rokka_stack.' . $entity->getName())
      ->delete();
  }

}
