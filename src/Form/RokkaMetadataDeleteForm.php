<?php

declare(strict_types=1);

namespace Drupal\rokka\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Rokka Metadata entities.
 *
 * @ingroup rokka
 */
class RokkaMetadataDeleteForm extends ContentEntityDeleteForm {


}
