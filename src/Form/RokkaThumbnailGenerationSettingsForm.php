<?php

declare(strict_types=1);

namespace Drupal\rokka\Form;

use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\Messenger;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Rokka thumbnail generation settings form.
 */
class RokkaThumbnailGenerationSettingsForm extends FormBase {


  /**
   * Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Messenger.
   *
   * @var \Drupal\Core\Messenger\Messenger
   */
  protected $messenger;

  /**
   * Config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactory
   */
  protected $configFactory;

  /**
   * SettingsForm constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   Entity type manager.
   * @param \Drupal\Core\Messenger\Messenger $messenger
   *   Messenger.
   * @param \Drupal\Core\Config\ConfigFactory $configFactory
   *   Config factory.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager, Messenger $messenger, ConfigFactory $configFactory) {
    $this->entityTypeManager = $entityTypeManager;
    $this->messenger = $messenger;
    $this->configFactory = $configFactory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('messenger'),
      $container->get('config.factory')
    );
  }

  /**
   * RegenerateThumbnails.
   *
   * @param mixed $mid
   *   Unknown.
   * @param mixed $context
   *   Unknown.
   */
  public static function regenerateThumbnail($mid, &$context): void {
    // FIXME: DI.
    $media = \Drupal::entityTypeManager()->getStorage('media')->load($mid);
    $media->save();
    $context['message'] = 'Processing - ' . $media->label();
    $context['results'][] = $media->label();
  }

  /**
   * Finish callback.
   *
   * @param mixed $success
   *   Success.
   * @param mixed $results
   *   Results.
   * @param mixed $operations
   *   Operations.
   */
  public static function finishedCallback($success, $results, $operations): void {
    if ($success) {
      $message = \Drupal::translation()->formatPlural(
        count($results),
        'One task processed.',
        '@count tasks processed.'
      );
      \Drupal::messenger()->addMessage($message);
    }
    else {
      \Drupal::messenger()->addError(t('Finished with an error.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'thumbnail_generation_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Regenerate thumbnails'),
      '#description' => $this->t('Regenerate thumbnail images for all media entities.'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $data = $this->configFactory->get('rokka.bundles.settings')->getRawData();
    $bundles = [];

    foreach ($data as $name => $item) {
      $length = strpos($name, '_field') ? strpos($name, '_field') : 0;
      $bundle = substr($name, 0, $length);
      if (!empty($data[$bundle . '_enable'])) {
        $bundles[] = $bundle;
      }
    }

    if (empty($bundles)) {
      return;
    }

    $mids = $this->entityTypeManager->getStorage('media')
      ->getQuery()
      ->accessCheck(TRUE)
      ->condition('bundle', $bundles, 'IN')
      ->execute();

    $operations = [];

    foreach ($mids as $mid) {
      $operations[] = [
        'Drupal\rokka\Form\RokkaThumbnailGenerationSettingsForm::regenerateThumbnail',
        [$mid],
      ];
    }

    $batch = [
      'title' => $this->t('Regenerates media thumbnails'),
      'operations' => $operations,
      'init_message' => $this->t('Thumbnail creating process is starting.'),
      'progress_message' => $this->t('Processed @current out of @total. Estimated time: @estimate.'),
      'error_message' => $this->t('An error occurred during processing'),
      'finished' => '\Drupal\rokka\Form\RokkaThumbnailGenerationSettingsForm::finishedCallback',
    ];

    $batch['operations'] = batch_set($batch);
  }

}
