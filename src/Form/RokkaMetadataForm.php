<?php

declare(strict_types=1);

namespace Drupal\rokka\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for Rokka Metadata edit forms.
 *
 * @ingroup rokka
 */
class RokkaMetadataForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $entity = &$this->entity;

    $status = parent::save($form, $form_state);

    switch ($status) {
      case SAVED_NEW:
        $this->messenger()->addMessage($this->t('Created the %label Rokka Metadata.', [
          '%label' => $entity->label(),
        ]));
        break;

      default:
        $this->messenger()->addMessage($this->t('Saved the %label Rokka Metadata.', [
          '%label' => $entity->label(),
        ]));
    }
    $form_state->setRedirect('entity.rokka_metadata.canonical', ['rokka_metadata' => $entity->id()]);
  }

}
