<?php

declare(strict_types=1);

namespace Drupal\rokka\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure Rokka settings for this site.
 */
class RokkaSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'rokka_admin_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $config = $this->config('rokka.settings');
    $form['is_enabled'] = [
      '#title' => $this->t('Enable Rokka.io service'),
      '#description' => $this->t('Enable or disable the Rokka.io integration'),
      '#type' => 'checkbox',
      '#default_value' => $config->get('is_enabled'),
    ];
    $form['credentials'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('API Credentials'),
      '#description' => $this->t('Enter your Rokka.io API credentials'),
      '#collapsible' => FALSE,
      'api_key' => [
        '#title' => $this->t('API Key'),
        '#description' => $this->t('The API Key credential provided by the Rokka.io service'),
        '#type' => 'textfield',
        '#required' => TRUE,
        '#default_value' => $config->get('api_key'),
      ],
    ];
    $form['organization'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Organization Credentials'),
      '#description' => $this->t('Enter the Organization at Rokka.io'),
      '#collapsible' => FALSE,

      'organization_name' => [
        '#title' => $this->t('Organization Name'),
        '#description' => $this->t('The Organization Name given from the Rokka.io service'),
        '#type' => 'textfield',
        '#required' => TRUE,
        '#default_value' => $config->get('organization_name'),
      ],
    ];
    $form['stack_default_settings'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Rokka Stack: Default Values'),
      '#description' => $this->t('These values will be used, when creating new stacks or the value is not set.'),
      '#collapsible' => FALSE,

      'jpg_quality' => [
        '#type' => 'number',
        '#title' => $this->t('JPG quality'),
        '#description' => $this->t(
          'Example values:<ul>
            <li>0 (Determined by Rokka)</li>
            <li>1 (highest compression, lowest quality)</li>
            <li>100 (lowest compression, highest quality)</li></ul>'
        ),
        '#min' => 0,
        '#max' => 100,
        '#default_value' => $config->get('jpg_quality') ?? 0,
      ],
      'webp_quality' => [
        '#type' => 'number',
        '#title' => $this->t('WEBP quality'),
        '#description' => $this->t(
          'Example values:<ul>
            <li>0 (Determined by Rokka)</li>
            <li>1 (highest compression, lowest quality)</li>
            <li>100 (lowest compression, highest quality)</li></ul>'
        ),
        '#min' => 0,
        '#max' => 100,
        '#default_value' => $config->get('webp_quality') ?? 0,
      ],
      'output_format' => [
        '#type' => 'select',
        '#title' => $this->t('Output format'),
        '#description' => $this->t('Defines the delivered image format.'),
        '#required' => TRUE,
        '#default_value' => $config->get('output_format') ?? 'jpg',
        '#options' => [
          'jpg' => $this->t('JPG'),
          'png' => $this->t('PNG'),
          'gif' => $this->t('GIF'),
        ],
      ],
      'autoformat' => [
        '#type' => 'checkbox',
        '#title' => $this->t('Autoformat'),
        '#description' => $this->t('Autoformat lets Rokka chose the best format supported by the browser.'),
        '#default_value' => $config->get('autoformat') ?? FALSE,
      ],
    ];
    $form['api_endpoint'] = [
      '#title' => $this->t('API Endpoint'),
      '#description' => $this->t('The API endpoint'),
      '#type' => 'textfield',
      '#required' => TRUE,
      '#default_value' => $config->get('api_endpoint'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $config = $this->config('rokka.settings');

    $config->set('is_enabled', $values['is_enabled']);
    $config->set('api_key', $values['api_key']);
    $config->set('api_endpoint', $values['api_endpoint']);
    $config->set('jpg_quality', $values['jpg_quality']);
    $config->set('webp_quality', $values['webp_quality']);
    $config->set('autoformat', $values['autoformat']);
    $config->set('output_format', $values['output_format']);
    $config->set('organization_name', $values['organization_name']);
    $config->save();

    parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['rokka.settings'];
  }

}
