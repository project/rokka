<?php

declare(strict_types=1);

namespace Drupal\rokka\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityFieldManager;
use Drupal\Core\Entity\EntityTypeBundleInfo;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure example settings for this site.
 */
class RokkaBundlesSettingsForm extends ConfigFormBase {

  /**
   * Config settings.
   *
   * @var string
   */
  protected const SETTINGS = 'rokka.bundles.settings';

  /**
   * Entity type bundle info.
   *
   * @var \Drupal\Core\Entity\EntityTypeBundleInfo
   */
  protected $entityTypeBundleInfo;

  /**
   * Entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManager
   */
  protected $entityFieldManager;

  /**
   * RokkaBundlesSettingsForm constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   Config factory.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfo $entityTypeBundleInfo
   *   Entity type bundle info.
   * @param \Drupal\Core\Entity\EntityFieldManager $entityFieldManager
   *   Entity field manager.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    EntityTypeBundleInfo $entityTypeBundleInfo,
    EntityFieldManager $entityFieldManager,
  ) {
    parent::__construct($config_factory);
    $this->entityTypeBundleInfo = $entityTypeBundleInfo;
    $this->entityFieldManager = $entityFieldManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('entity_type.bundle.info'),
      $container->get('entity_field.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'rokka_bundles_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $config = $this->config(static::SETTINGS);

    foreach ($this->getFieldsList() as $bundleId => $infos) {
      if (!empty($infos['fields'])) {
        $form[$bundleId] = [
          '#type' => 'fieldset',
          '#title' => $infos['label'],
          '#collapsible' => FALSE,
          '#collapsed' => FALSE,
        ];

        $form[$bundleId][$bundleId . '_field'] = [
          '#type' => 'select',
          '#title' => $this->t('Field to use to generate thumbnail'),
          '#description' => $this->t(
            'The file attached must be a supported type (e.g. pdf), otherwise it will be ignored.</br>If that field is multivalued, only the first value will be used.'
          ),
          '#options' => $infos['fields'],
          '#default_value' => $config->get($bundleId . '_field'),
        ];

        $form[$bundleId][$bundleId . '_enable'] = [
          '#type' => 'checkbox',
          '#title' => $this->t('Enable'),
          '#default_value' => $config->get($bundleId . '_enable'),
        ];
      }
    }
    return parent::buildForm($form, $form_state);
  }

  /**
   * Get Field list.
   *
   * @return array
   *   Field list.
   */
  protected function getFieldsList(): array {
    $bundles = $this->entityTypeBundleInfo->getBundleInfo('media');
    $output = [];
    foreach ($bundles as $id => $bundle) {
      $output[$id]['label'] = $bundle['label'];
      foreach ($this->entityFieldManager->getFieldDefinitions('media', $id) as $fieldDefinition) {
        if ($fieldDefinition->getType() === 'file') {
          $output[$id]['fields'][$fieldDefinition->getName()] = $fieldDefinition->getName();
        }
      }
    }
    return $output;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $editableConfig = $this->configFactory->getEditable(static::SETTINGS);
    foreach ($this->getFieldsList() as $bundleId => $infos) {
      $editableConfig->set($bundleId . '_field', $form_state->getValue($bundleId . '_field'));
      $editableConfig->set($bundleId . '_enable', $form_state->getValue($bundleId . '_enable'));
    }
    $editableConfig->save();
    parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return [
      static::SETTINGS,
    ];
  }

}
