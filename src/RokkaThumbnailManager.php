<?php

declare(strict_types=1);

namespace Drupal\rokka;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\State\StateInterface;

/**
 * Rokka thumbnail manager.
 */
class RokkaThumbnailManager {

  /**
   * EntityTypeManager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   * FileSystem.
   *
   * @var \Drupal\Core\File\FileSystem
   */
  protected $fileSystem;

  /**
   * Config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactory
   */
  protected $configFactory;

  /**
   * State.
   *
   * @var \Drupal\Core\State\State
   */
  protected $state;

  /**
   * RokkaThumbnailManager constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   Entity Type manager.
   * @param \Drupal\Core\File\FileSystemInterface $fileSystem
   *   Filesystem.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   Config Factory.
   * @param \Drupal\workflows\StateInterface $state
   *   State.
   */
  public function __construct(
    EntityTypeManagerInterface $entityTypeManager,
    FileSystemInterface $fileSystem,
    ConfigFactoryInterface $configFactory,
    StateInterface $state,
  ) {
    $this->entityTypeManager = $entityTypeManager;
    $this->fileSystem = $fileSystem;
    $this->configFactory = $configFactory;
    $this->state = $state;
  }

  /**
   * Create pdf thumbnail.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   Entity.
   */
  public function createThumbnail(EntityInterface $entity): void {
    if ($entity->getEntityTypeId() !== 'media') {
      return;
    }

    $bundle = $entity->bundle();
    $config = $this->configFactory->get('rokka.bundles.settings');
    // If not enabled for this bundle.
    if (empty($config->get($bundle . '_enable'))) {
      return;
    }

    $fieldName = $config->get($bundle . '_field');
    if ($fieldName && $entity->hasField($fieldName) && !empty($entity->get($fieldName)->getValue())) {
      $fid = $entity->get($fieldName)->getValue()[0]['target_id'];
      /** @var \Drupal\file\FileInterface $fileEntity */
      $fileEntity = $this->entityTypeManager->getStorage('file')->load($fid);
      if ($fileEntity) {
        $entity->set(
          'thumbnail',
          [
            'target_id' => $fileEntity->id(),
            'alt' => $fileEntity->getFilename(),
          ]
        );
      }

      // On default language, also update translations.
      // This omits a bug, where the thumbnail is lost on translations
      // after updating the main language.
      $default_language = \Drupal::languageManager()->getDefaultLanguage()->getId();
      if ($entity->language()->getId() === $default_language) {
        $languages = \Drupal::languageManager()->getLanguages();
        $langcodes = array_keys($languages);

        // Update translations.
        foreach ($langcodes as $langcode) {
          if ($langcode == $default_language) {
            continue;
          }
          if ($entity->hasTranslation($langcode)) {
            $translation = $entity->getTranslation($langcode);
            $fid = $translation->get($fieldName)->getValue()[0]['target_id'];
            /** @var \Drupal\file\FileInterface $fileEntity */
            $fileEntity = $this->entityTypeManager->getStorage('file')->load($fid);
            if ($fileEntity) {
              $translation->set(
                'thumbnail',
                [
                  'target_id' => $fileEntity->id(),
                  'alt' => $fileEntity->getFilename(),
                ]
              );
            }
          }
        }
      }
    }
  }

}
