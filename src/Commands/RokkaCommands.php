<?php

declare(strict_types=1);

namespace Drupal\rokka\Commands;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\rokka\Entity\RokkaStack;
use Drush\Commands\DrushCommands;

/**
 * Rokka Drush commands.
 */
class RokkaCommands extends DrushCommands {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a new RokkaCommands instance.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity manager service.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct();
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * Migrate existing imagestyles to rokka stacks.
   *
   * @command rokka:migrate-imagestyles
   * @option force
   * @aliases rim, rokka-mim
   */
  public function migrateImageStyles($options = ['force' => FALSE]): void {
    $this->output()->writeln('Migrating local image styles to rokka.io stacks.');
    $styles = $this->entityTypeManager->getStorage('image_style')->loadMultiple();
    $stacks = RokkaStack::loadMultiple();

    // Find all and create all missing stacks on rokka.io.
    foreach ($styles as $style) {
      /** @var \Drupal\image\Entity\ImageStyle $style */
      if (empty($stacks[$style->getName()]) || $options['force']) {
        // Create stack.
        try {
          rokka_image_style_presave($style);
          $this->logger()->success(
            dt(
              'Image style !style_name stack created on rokka.io',
              ['!style_name' => $style->getName()]
            )
          );
        }
        catch (\Exception $ex) {
          $this->logger()->error(
            dt(
              'Image style !style_name stack failed to save on rokka.io',
              ['!style_name' => $style->getName()]
            )
          );
        }
      }
    }
    $this->output()->writeln('Image styles migration completed.');
  }

}
