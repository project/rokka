<?php

declare(strict_types=1);

namespace Drupal\rokka;

use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\rokka\Entity\RokkaStack;
use Psr\Log\LoggerInterface;
use Rokka\Client\Base;
use Rokka\Client\Factory;
use Rokka\Client\Image;
use Rokka\Client\TemplateHelper;
use Rokka\Client\User;

/**
 * Defines a RokkaService service.
 */
class RokkaService implements RokkaServiceInterface {

  /**
   * Logger.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * Config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactory
   */
  protected $configFactory;

  /**
   * API Key.
   *
   * @var array|mixed|null
   */
  private $apiKey;

  /**
   * Organization name.
   *
   * @var array|mixed|null
   */
  private $organizationName;

  /**
   * API Endpoint.
   *
   * @var array|mixed|string|null
   */
  private $apiEndpoint;

  /**
   * Rokka stack storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $storageRokkaStack;

  /**
   * Rokka metadata storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $storageRokkaMetadata;

  /**
   * RokkaService constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Entity type manager.
   * @param \Drupal\Core\Config\ConfigFactory $config_factory
   *   Config factory.
   * @param \Psr\Log\LoggerInterface $logger
   *   Logger.
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager,
    ConfigFactory $config_factory,
    LoggerInterface $logger,
  ) {
    $this->storageRokkaStack = $entity_type_manager->getStorage('rokka_stack');
    $this->storageRokkaMetadata = $entity_type_manager->getStorage('rokka_metadata');
    $this->logger = $logger;
    $this->configFactory = $config_factory;

    $config = $config_factory->get('rokka.settings');

    $this->apiKey = $config->get('api_key');
    $this->organizationName = $config->get('organization_name');
    $this->apiEndpoint = $config->get('api_endpoint') ?: Base::DEFAULT_API_BASE_URL;
  }

  /**
   * Returns the SEO compliant filename for the given image name.
   *
   * @param string $filename
   *   Filename.
   *
   * @return string
   *   Filename.
   */
  public static function cleanRokkaSeoFilename($filename): string {
    // Rokka.io accepts SEO URL part as "[a-z0-9-]" only, remove not valid
    // characters and replace them with '-'.
    return TemplateHelper::slugify($filename);
  }

  /**
   * {@inheritdoc}
   */
  public function getRokkaImageClient(): Image {
    return Factory::getImageClient($this->organizationName, $this->apiKey, '', $this->apiEndpoint);
  }

  /**
   * {@inheritdoc}
   */
  public function getRokkaUserClient(): User {
    return Factory::getUserClient($this->apiEndpoint);
  }

  /**
   * {@inheritdoc}
   */
  public function getRokkaOrganizationName() {
    return $this->organizationName;
  }

  /**
   * {@inheritdoc}
   */
  public function loadRokkaMetadataByUri(string $uri): array {
    return $this->storageRokkaMetadata
      ->loadByProperties(['uri' => $uri]);
  }

  /**
   * Load Rokka metadata by binary hash.
   *
   * @param string $binary_hash
   *   Hash.
   *
   * @return \Drupal\Core\Entity\EntityInterface[]
   *   Entities.
   */
  public function loadRokkaMetadataByBinaryHash(string $binary_hash): array {
    return $this->storageRokkaMetadata
      ->loadByProperties(['binary_hash' => $binary_hash]);
  }

  /**
   * Load stack by name.
   *
   * @param string $name
   *   Name.
   *
   * @return \Drupal\rokka\Entity\RokkaStack|null
   *   Stack.
   */
  public function loadStackByName(string $name): ?RokkaStack {
    /** @var \Drupal\rokka\Entity\RokkaStack $stack */
    $stack = $this->storageRokkaStack->load($name);
    return $stack;
  }

  /**
   * {@inheritdoc}
   */
  public function countImagesWithHash(string $hash): int {
    return (int) $this->storageRokkaMetadata
      ->getQuery()
      ->accessCheck(FALSE)
      ->condition('hash', $hash)
      ->count()
      ->execute();
  }

}
