<?php

declare(strict_types=1);

namespace Drupal\rokka;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\crop\CropInterface;
use GuzzleHttp\Exception\ClientException;
use Rokka\Client\Core\DynamicMetadata\CropArea;
use Rokka\Client\Core\DynamicMetadata\MultiAreas;
use Rokka\Client\Core\DynamicMetadata\SubjectArea;

/**
 * Crop update service helper.
 */
class CropHelper {

  /**
   * Rokka service.
   *
   * @var \Drupal\rokka\RokkaServiceInterface
   */
  protected $rokkaService;

  /**
   * Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * StackHelper constructor.
   *
   * @param \Drupal\rokka\RokkaServiceInterface $rokkaService
   *   Rokka service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   Entity type manager.
   */
  public function __construct(RokkaServiceInterface $rokkaService, EntityTypeManagerInterface $entityTypeManager) {
    $this->rokkaService = $rokkaService;
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * Update cropping metadata in Rokka.
   *
   * @param \Drupal\crop\CropInterface $crop
   *   Crop.
   */
  public function updateCropMetadata(CropInterface $crop): void {
    $client = $this->rokkaService->getRokkaImageClient();

    // Special handling for focal points, they are not saved using MultiAreas.
    // Instead, they are used as the default SubjectArea for all crops that are
    // not using a named MultiArea.
    if ($crop->bundle() === 'focal_point') {
      $coords = $crop->anchor();
      $dynamic_md = new SubjectArea($coords['x'], $coords['y'], 1, 1);
    }
    else {
      // For multiarea crops, we need to send all custom crops on the image in
      // one single structure, so load them all first.
      $crop_storage = $this->entityTypeManager->getStorage('crop');

      $image_crops = $crop_storage->loadByProperties(
        ['uri' => $crop->uri->value]
      );

      $areas = [];

      foreach ($image_crops as $image_crop) {
        // Skip focal_point crops here, these are handled above.
        if ($image_crop->bundle() === 'focal_point') {
          continue;
        }

        $coords = $image_crop->anchor();
        $size = $image_crop->size();

        $area_crop = new CropArea(
          $coords['x'],
          $coords['y'],
          $size['width'],
          $size['height']
        );

        $areas[$image_crop->bundle()] = [$area_crop];
      }

      if (count($areas) > 0) {
        $dynamic_md = new MultiAreas($areas);
      }
    }

    if (!empty($dynamic_md)) {
      $metas = $this->rokkaService->loadRokkaMetadataByUri($crop->uri->value);
      foreach ($metas as $meta) {
        $old_hash = $meta->getHash();

        try {
          $new_hash = $client->setDynamicMetadata(
            $dynamic_md,
            $old_hash,
            NULL,
            ['deletePrevious' => TRUE]
          );
          if ($new_hash) {
            // Check if the old hash has been updated.
            if ($meta->getHash() !== $new_hash) {
              $meta->hash = $new_hash;
              $meta->save();
            }
          }
        }
        catch (ClientException $client_exception) {
          // Fail silently if image has been deleted from Rokka.
          $code = $client_exception->getCode();
          if ($code == 404 || $code == 403) {
            // @todo Inject.
            \Drupal::logger('rokka')->error(
              'Could not update metadata for @uri',
              ['@uri' => $crop->uri->value]
            );
            continue;
          }

          throw $client_exception;
        }
      }
    }
  }

}
