<?php

declare(strict_types=1);

namespace Drupal\rokka;

use Drupal\rokka\StyleEffects\ImageEffectInterface;

/**
 * Image style helper.
 */
class ImageStyleHelper {

  /**
   * Returns the Angle value in [0-360] interval.
   *
   * @param int $angle
   *   Angle.
   *
   * @return int
   *   Angle.
   */
  public static function operationNormalizeAngle(int $angle): int {
    $angle %= 360;
    if ($angle < 0) {
      $angle = 360 + $angle;
    }
    return $angle;
  }

  /**
   * Normalize color.
   *
   * @param mixed $value
   *   Color.
   *
   * @return mixed
   *   Color without pound key.
   */
  public static function operationNormalizeColor($value) {
    return str_replace('#', '', $value);
  }

  /**
   * Normalize size.
   *
   * @param mixed $value
   *   Size.
   *
   * @return mixed
   *   Size.
   */
  public static function operationNormalizeSize($value) {
    $value = $value ?: PHP_INT_MAX;
    return min(10000, max(1, $value));
  }

  /**
   * Build stack operation collection.
   *
   * @todo Redundant functionality in StackHelper::buildStackOperationCollection.
   *
   * @param array $effects
   *   Effects.
   *
   * @return \Rokka\Client\Core\StackOperation[]
   *   Stack operations.
   */
  public static function buildStackOperationCollection(array $effects): ?array {
    if (empty($effects)) {
      $effects = [
        [
          'name' => 'noop',
          'data' => NULL,
        ],
      ];
    }

    $operations = [];
    $currentId = 0;
    foreach ($effects as $effect) {
      $ops = static::buildStackOperation($effect);
      if (!empty($ops)) {
        foreach ($ops as $op) {
          $operations[$currentId++] = $op;
        }
      }
    }

    if (empty($operations)) {
      return NULL;
    }

    ksort($operations);
    return $operations;
  }

  /**
   * Build stack operation.
   *
   * @param array $effect
   *   Effect.
   *
   * @return \Rokka\Client\Core\StackOperation[]
   *   Stack operations.
   */
  private static function buildStackOperation(array $effect): array {
    $name = $effect['name'];
    $className = 'Drupal\rokka\StyleEffects\Effect' . static::camelCase($name, TRUE);

    $result = [];
    if (class_exists($className) && in_array(ImageEffectInterface::class, class_implements($className))) {
      /** @var \Drupal\rokka\StyleEffects\ImageEffectInterface $className */
      $result = $className::buildRokkaStackOperation($effect['data']);
    }
    else {
      // @todo Inject.
      \Drupal::logger('rokka')->error(
        'Can not convert effect "@effect" to Rokka.io StackOperation: "@class" Class missing!', [
          '@effect' => $name,
          '@class' => $className,
        ]
      );
    }

    return $result;
  }

  /**
   * Transform to camel case.
   *
   * @param string $str
   *   String.
   * @param bool $classCase
   *   Case.
   *
   * @return string
   *   String.
   */
  private static function camelCase(string $str, $classCase = FALSE): string {
    // non-alpha and non-numeric characters become spaces.
    $str = preg_replace('/[^a-z0-9]+/i', ' ', $str);
    $str = trim($str);
    // Uppercase the first character of each word.
    $str = ucwords($str);
    $str = str_replace(' ', '', $str);
    if (!$classCase) {
      $str = lcfirst($str);
    }
    return $str;
  }

}
