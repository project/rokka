<?php

declare(strict_types=1);

namespace Drupal\rokka\Plugin\ImageEffect;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Image\ImageInterface;
use Drupal\image\ConfigurableImageEffectBase;

/**
 * Image composition function.
 *
 * @ImageEffect(
 *   id = "rokka_image_effects_composition",
 *   label = @Translation("Rokka: Image Composition"),
 *   description = @Translation("Overlay the image with another image.")
 * )
 */
class RokkaCompositionImageEffect extends ConfigurableImageEffectBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'width' => NULL,
      'height' => NULL,
      'secondary_opacity' => 100,
      'angle' => 0,
      'mode' => 'background',
      'resize_mode' => 'box',
      'anchor' => 'center_center',
      'resize_to_primary' => FALSE,
      'secondary_color' => '000000',
      'secondary_image' => '',
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form['width'] = [
      '#type' => 'number',
      '#title' => $this->t('Width'),
      '#default_value' => $this->configuration['width'],
      '#field_suffix' => $this->t('px'),
      '#size' => 12,
    ];
    $form['height'] = [
      '#type' => 'number',
      '#title' => $this->t('Height'),
      '#default_value' => $this->configuration['height'],
      '#field_suffix' => $this->t('px'),
      '#size' => 12,
    ];
    $form['secondary_opacity'] = [
      '#type' => 'number',
      '#title' => $this->t('Secondary Opacity'),
      '#default_value' => $this->configuration['secondary_opacity'],
      '#field_suffix' => $this->t('%'),
      '#required' => TRUE,
      '#size' => 3,
      '#min' => 0,
      '#max' => 100,
    ];
    $form['angle'] = [
      '#type' => 'number',
      '#title' => $this->t('Angle'),
      '#default_value' => $this->configuration['angle'],
      '#field_suffix' => $this->t('degree'),
      '#required' => TRUE,
      '#size' => 3,
      '#min' => 0,
      '#max' => 360,
    ];
    $form['mode'] = [
      '#type' => 'select',
      '#title' => $this->t('Mode'),
      '#description' => $this->t('Where is the primary image placed?'),
      '#default_value' => $this->configuration['mode'],
      '#options' => [
        'background' => $this->t('Background'),
        'foreground' => $this->t('Foreground'),
      ],
      '#required' => TRUE,
    ];
    $form['resize_mode'] = [
      '#type' => 'select',
      '#title' => $this->t('Resize Mode'),
      '#description' => $this->t('Default: box'),
      '#default_value' => $this->configuration['resize_mode'],
      '#options' => [
        'box' => $this->t('Box'),
        'absolute' => $this->t('Absolute'),
        'fill' => $this->t('Fill'),
      ],
      '#required' => TRUE,
    ];
    $form['resize_to_primary'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Resize to Primary'),
      '#default_value' => $this->configuration['resize_to_primary'],
    ];
    $form['anchor'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Anchor'),
      '#description' => $this->t('auto or XOFFSET_YOFFSET. Example: right_bottom / left_top or 20_590'),
      '#default_value' => $this->configuration['anchor'],
      '#required' => TRUE,
    ];
    $form['secondary_image'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Secondary Image Hash'),
      '#description' => $this->t('Upload an image file to rokka.io and copy the hash value into this field.'),
      '#default_value' => $this->configuration['secondary_image'],
      '#required' => TRUE,
    ];
    $form['secondary_color'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Secondary Color'),
      '#description' => $this->t('Hex Code without # sign.'),
      '#default_value' => $this->configuration['secondary_color'],
      '#field_prefix' => $this->t('#'),
      '#required' => TRUE,
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    $this->configuration['width'] = $form_state->getValue('width');
    $this->configuration['height'] = $form_state->getValue('height');
    $this->configuration['secondary_opacity'] = $form_state->getValue('secondary_opacity');
    $this->configuration['angle'] = $form_state->getValue('angle');
    $this->configuration['mode'] = $form_state->getValue('mode');
    $this->configuration['resize_to_primary'] = $form_state->getValue('resize_to_primary');
    $this->configuration['resize_mode'] = $form_state->getValue('resize_mode');
    $this->configuration['anchor'] = $form_state->getValue('anchor');
    $this->configuration['secondary_image'] = $form_state->getValue('secondary_image');
    $this->configuration['secondary_color'] = $form_state->getValue('secondary_color');
  }

  /**
   * {@inheritdoc}
   */
  public function applyEffect(ImageInterface $image) {
    return $image;
  }

}
