<?php

declare(strict_types=1);

namespace Drupal\rokka\Plugin\ImageEffect;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Image\ImageInterface;
use Drupal\image\ConfigurableImageEffectBase;

/**
 * Text overlay function.
 *
 * @ImageEffect(
 *   id = "rokka_image_effects_text_overlay",
 *   label = @Translation("Rokka: Text Overlay"),
 *   description = @Translation("Overlay the image with text.")
 * )
 */
class RokkaTextOverlayImageEffect extends ConfigurableImageEffectBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration(): array {
    return [
      'size' => 40,
      'opacity' => 100,
      'angle' => 0,
      'width' => NULL,
      'height' => NULL,
      'resize_to_box' => FALSE,
      'text' => '',
      'font' => '',
      'anchor' => 'auto',
      'color' => 'FFFFFF',
      'align' => 'left',
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state): array {
    $form['size'] = [
      '#type' => 'number',
      '#title' => $this->t('Font Size'),
      '#default_value' => $this->configuration['size'],
      '#field_suffix' => $this->t('px'),
      '#required' => TRUE,
      '#size' => 3,
    ];
    $form['opacity'] = [
      '#type' => 'number',
      '#title' => $this->t('Opacity'),
      '#default_value' => $this->configuration['opacity'],
      '#field_suffix' => $this->t('%'),
      '#required' => TRUE,
      '#size' => 3,
      '#min' => 0,
      '#max' => 100,
    ];
    $form['angle'] = [
      '#type' => 'number',
      '#title' => $this->t('Angle'),
      '#default_value' => $this->configuration['angle'],
      '#field_suffix' => $this->t('degree'),
      '#required' => TRUE,
      '#size' => 3,
      '#min' => 0,
      '#max' => 360,
    ];
    $form['width'] = [
      '#type' => 'number',
      '#title' => $this->t('Width'),
      '#default_value' => $this->configuration['width'],
      '#field_suffix' => $this->t('px'),
      '#size' => 3,
    ];
    $form['height'] = [
      '#type' => 'number',
      '#title' => $this->t('Height'),
      '#default_value' => $this->configuration['height'],
      '#field_suffix' => $this->t('px'),
      '#size' => 3,
    ];
    $form['resize_to_box'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Resize To Box'),
      '#default_value' => $this->configuration['resize_to_box'],
      '#description' => $this->t('If you specified a width and a height, the text will be automatically fit into the box.'),
    ];
    $form['text'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Text'),
      '#default_value' => $this->configuration['text'],
      '#required' => TRUE,
    ];
    $form['font'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Font Rokka Hash'),
      '#description' => $this->t('Upload a ttf font file to rokka.io and copy the hash value into this field.'),
      '#default_value' => $this->configuration['font'],
      '#required' => TRUE,
    ];
    $form['anchor'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Anchor'),
      '#description' => $this->t('auto or XOFFSET_YOFFSET. Example: right_bottom / left_top or 20_590'),
      '#default_value' => $this->configuration['anchor'],
      '#required' => TRUE,
    ];
    $form['color'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Color'),
      '#description' => $this->t('Hex Code without # sign.'),
      '#default_value' => $this->configuration['color'],
      '#field_prefix' => $this->t('#'),
      '#required' => TRUE,
    ];
    $form['align'] = [
      '#type' => 'select',
      '#title' => $this->t('Text align'),
      '#description' => $this->t('How to align text if width us given.'),
      '#default_value' => $this->configuration['align'],
      '#options' => [
        'left' => $this->t('Left'),
        // @todo BE/AE.
        'centre' => $this->t('Center'),
        'right' => $this->t('Right'),
      ],
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    $this->configuration['size'] = $form_state->getValue('size');
    $this->configuration['opacity'] = $form_state->getValue('opacity');
    $this->configuration['angle'] = $form_state->getValue('angle');
    $this->configuration['width'] = $form_state->getValue('width');
    $this->configuration['height'] = $form_state->getValue('height');
    $this->configuration['resize_to_box'] = $form_state->getValue('resize_to_box');
    $this->configuration['text'] = $form_state->getValue('text');
    $this->configuration['font'] = $form_state->getValue('font');
    $this->configuration['anchor'] = $form_state->getValue('anchor');
    $this->configuration['color'] = $form_state->getValue('color');
  }

  /**
   * {@inheritdoc}
   */
  public function applyEffect(ImageInterface $image) {
    return $image;
  }

}
