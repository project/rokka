<?php

declare(strict_types=1);

namespace Drupal\rokka\Plugin\Field\FieldFormatter;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\file\Plugin\Field\FieldFormatter\FileMediaFormatterBase;
use Drupal\rokka\RokkaServiceInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'file_video' formatter.
 *
 * @FieldFormatter(
 *   id = "rokka_video_hls",
 *   label = @Translation("Rokka HLS Video"),
 *   description = @Translation("Display the file using an adaptive rate client."),
 *   field_types = {
 *     "file"
 *   }
 * )
 */
class HlsVideoFormatter extends FileMediaFormatterBase {

  /**
   * Rokka service.
   *
   * @var \Drupal\rokka\RokkaServiceInterface
   */
  protected $rokkaService;

  /**
   * Constructs an VideoPlayerFormatter object.
   *
   * @param string $plugin_id
   *   The plugin_id for the formatter.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the formatter is associated.
   * @param array $settings
   *   The formatter settings.
   * @param string $label
   *   The formatter label display setting.
   * @param string $view_mode
   *   The view mode.
   * @param array $third_party_settings
   *   Any third party settings settings.
   * @param \Drupal\rokka\RokkaServiceInterface $rokka_service
   *   The Rokka service.
   */
  public function __construct(
    $plugin_id,
    $plugin_definition,
    FieldDefinitionInterface $field_definition,
    array $settings,
    $label,
    $view_mode,
    array $third_party_settings,
    RokkaServiceInterface $rokka_service,
  ) {
    parent::__construct(
      $plugin_id,
      $plugin_definition,
      $field_definition,
      $settings,
      $label,
      $view_mode,
      $third_party_settings
    );
    $this->rokkaService = $rokka_service;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition,
  ) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $container->get('rokka.service')
    );
  }

  /**
   * {@inheritdoc}
   */
  public static function getMediaType(): string {
    return 'video';
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings(): array {
    return [
      'muted' => FALSE,
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    return [
      'controls' => [
        '#title' => $this->t('Show playback controls'),
        '#type' => 'checkbox',
        '#default_value' => $this->getSetting('controls'),
      ],
      'autoplay' => [
        '#title' => $this->t('Autoplay'),
        '#type' => 'checkbox',
        '#default_value' => $this->getSetting('autoplay'),
      ],
      'loop' => [
        '#title' => $this->t('Loop'),
        '#type' => 'checkbox',
        '#default_value' => $this->getSetting('loop'),
      ],
      'muted' => [
        '#title' => $this->t('Muted'),
        '#type' => 'checkbox',
        '#default_value' => $this->getSetting('muted'),
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];
    $summary[] = $this->t('Playback controls: %controls', ['%controls' => $this->getSetting('controls') ? $this->t('visible') : $this->t('hidden')]);
    $summary[] = $this->t('Autoplay: %autoplay', ['%autoplay' => $this->getSetting('autoplay') ? $this->t('yes') : $this->t('no')]);
    $summary[] = $this->t('Loop: %loop', ['%loop' => $this->getSetting('loop') ? $this->t('yes') : $this->t('no')]);
    $summary[] = $this->t('Muted: %muted', ['%muted' => $this->getSetting('muted') ? $this->t('yes') : $this->t('no')]);
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  protected function prepareAttributes(array $additional_attributes = []) {
    return parent::prepareAttributes(['muted']);
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode): array {
    $source_files = $this->getSourceFiles($items, $langcode);
    if (empty($source_files)) {
      return [];
    }

    $elements = [];
    $attributes = $this->prepareAttributes();

    foreach ($source_files as $delta => $files) {
      /** @var \Drupal\file\Entity\File $file */
      // Multiple videos on a field are supported but only one per media entity.
      $file = $files[0]['file'];
      $rokka_metadata = $this->rokkaService->loadRokkaMetadataByUri($file->getFileUri());
      $rokka_metadata = reset($rokka_metadata);
      $client = $this->rokkaService->getRokkaImageClient();
      $elements[$delta] = [
        '#theme' => $this->getPluginId(),
        '#attributes' => $attributes,
        '#video' => [
          'id' => $file->id(),
          'width' => $rokka_metadata->getWidth(),
          'height' => $rokka_metadata->getHeight(),
          'uri' => $client->getSourceImageUri($rokka_metadata->getHash(), 'dynamic', 'm3u'),
          'poster' => $client->getSourceImageUri($rokka_metadata->getHash(), 'dynamic', 'jpg'),
        ],
        '#cache' => ['tags' => []],
        '#attached' => ['library' => ['rokka/videojs']],
      ];

      $cache_tags = [];
      $cache_tags = Cache::mergeTags($cache_tags, $file->getCacheTags());
      $elements[$delta]['#cache']['tags'] = $cache_tags;
    }

    return $elements;
  }

}
