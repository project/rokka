<?php

declare(strict_types=1);

namespace Drupal\rokka\Plugin\ImageToolkit;

use Drupal\system\Plugin\ImageToolkit\GDToolkit;

/**
 * Provides ImageMagick integration toolkit for image manipulation.
 *
 * @ImageToolkit(
 *   id = "rokka",
 *   title = @Translation("Rokka image toolkit")
 * )
 */
class RokkaToolkit extends GDToolkit {

  /**
   * {@inheritdoc}
   */
  public function getBaseId() {
    return 'gd';
  }

  /**
   * {@inheritdoc}
   */
  public function isValid() {
    // We assume that all successfully uploaded images are valid.
    if (!empty($this->getSource()) && strpos($this->getSource(), 'rokka://') === FALSE) {
      // This case happens during upload to /tmp folder before the file is
      // transferred to rokka.io.
      return ((bool) $this->getMimeType());
    }
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function getHeight() {
    if (!empty($this->getSource()) && strpos($this->getSource(), 'rokka://') === 0) {
      // Use the cached meta data from the rokka metadata table.
      // @todo Fix DI.
      $metadata = \Drupal::service('rokka.service')->loadRokkaMetadataByUri($this->getSource());
      if (!empty($metadata)) {
        $metadata = reset($metadata);
        return $metadata->getHeight();
      }
    }
    return parent::getHeight();
  }

  /**
   * {@inheritdoc}
   */
  public function getWidth() {
    if (!empty($this->getSource()) && strpos($this->getSource(), 'rokka://') === 0) {
      // Use the cached meta data from the rokka metadata table.
      // @todo Fix DI.
      $metadata = \Drupal::service('rokka.service')->loadRokkaMetadataByUri($this->getSource());
      if (!empty($metadata)) {
        $metadata = reset($metadata);
        return $metadata->getWidth();
      }
    }
    return parent::getWidth();
  }

  /**
   * Get the mime type.
   *
   * @return string
   *   Mime type.
   */
  public function getMimeType(): string {
    $mime_type = $this->getType() ? image_type_to_mime_type($this->getType()) : '';

    // GD library does not support SVG, but Rokka does. So we will trick the
    // toolkit and detect SVG as a mime time the first time the file is uploaded
    // to the tmp folder. If we detect an SVG image, we allow the upload.
    if (empty($mime_type)) {
      $uri = $this->getSource();
      if ($uri && file_exists($uri) && strpos($uri, 'rokka://') === FALSE) {
        $rokka_additional_allowed_mime_types = ['image/svg', 'image/svg+xml'];
        $mime_type_guessed = mime_content_type($uri);
        if (in_array($mime_type_guessed, $rokka_additional_allowed_mime_types)) {
          $mime_type = $mime_type_guessed;
        }
      }
    }

    return $mime_type;
  }

  /**
   * {@inheritdoc}
   */
  public function parseFile(): bool {
    $source = $this->getSource();
    if (!empty($source) && strpos($source, 'rokka://') === 0) {
      $data = NULL;
      // Use the cached meta data from the rokka metadata table.
      $metadata = \Drupal::service('rokka.service')
        ->loadRokkaMetadataByUri($this->getSource());
      if (!empty($metadata)) {
        $metadata = reset($metadata);

        // Mimic the getimagesize function of php.
        $data[0] = $metadata->getWidth();
        $data[1] = $metadata->getHeight();
        $data[2] = $this->extensionToImageType($metadata->getFormat());
      }

      if ($data && in_array($data[2], static::supportedTypes(), TRUE)) {
        $this->setType($data[2]);
        $this->preLoadInfo = $data;
        return TRUE;
      }
      return FALSE;
    }

    // This case happens during upload to the /tmp folder before the file is
    // transferred to rokka.io.
    return parent::parseFile();
  }

}
