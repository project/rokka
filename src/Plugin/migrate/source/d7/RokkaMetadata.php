<?php

declare(strict_types=1);

namespace Drupal\rokka\Plugin\migrate\source\d7;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\State\StateInterface;
use Drupal\migrate\Plugin\MigrationInterface;
use Drupal\migrate\Row;
use Drupal\migrate_drupal\Plugin\migrate\source\DrupalSqlBase;
use Drupal\rokka\RokkaServiceInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Rokka metadata source plugin.
 *
 * @MigrateSource(
 *   id = "d7_rokka_metadata",
 *   source_module = "rokka"
 * )
 */
class RokkaMetadata extends DrupalSqlBase {

  /**
   * Rokka service.
   *
   * @var \Drupal\rokka\RokkaServiceInterface
   */
  protected $rokkaService;

  /**
   * Constructs a new RokkaMetadata migrate source plugin.
   *
   * @param array $configuration
   *   The plugin configuration.
   * @param string $plugin_id
   *   The plugin ID.
   * @param mixed $plugin_definition
   *   The plugin definition.
   * @param \Drupal\migrate\Plugin\MigrationInterface $migration
   *   The migration.
   * @param \Drupal\Core\State\StateInterface $state
   *   The state service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\rokka\RokkaServiceInterface $rokka_service
   *   The rokka service.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    MigrationInterface $migration,
    StateInterface $state,
    EntityTypeManagerInterface $entity_type_manager,
    RokkaServiceInterface $rokka_service,
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $migration, $state, $entity_type_manager);

    $this->rokkaService = $rokka_service;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition, MigrationInterface $migration = NULL) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $migration,
      $container->get('state'),
      $container->get('entity_type.manager'),
      $container->get('rokka.service')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
    return $this->select('rokka_metadata', 'rm')
      ->fields('rm', [
        'uri',
        'hash',
        'filesize',
        'created',
      ]);
  }

  /**
   * {@inheritdoc}
   */
  public function prepareRow(Row $row) {
    // Early skip.
    if (FALSE === parent::prepareRow($row)) {
      return FALSE;
    }

    $hash = $row->getSourceProperty('hash');

    // Gather missing infos form the service.
    try {
      /** @var \Rokka\Client\Core\SourceImage $sourceImage */
      $sourceImage = $this->rokkaService
        ->getRokkaImageClient()
        ->getSourceImage($hash);

      $row->setSourceProperty('binary_hash', $sourceImage->binaryHash);
      $row->setSourceProperty('height', $sourceImage->height);
      $row->setSourceProperty('width', $sourceImage->width);
      $row->setSourceProperty('format', $sourceImage->format);
    }
    catch (\Exception $exception) {
      $row->setSourceProperty('binary_hash', NULL);
      $row->setSourceProperty('height', NULL);
      $row->setSourceProperty('width', NULL);
      $row->setSourceProperty('format', NULL);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function fields() {
    return [
      'fid' => $this->t('The original file FID (unused).'),
      'hash' => $this->t('The Rokka.io hash (SHA1, 40 chars) of the file.'),
      'filesize' => $this->t('File size in octets.'),
      'created' => $this->t('Creation date timestamp.'),
      'uri' => $this->t('The original file URI.'),
      'binary_hash' => $this->t('Rokka.io service provided binary_hash.'),
      'height' => $this->t('Rokka.io service provided height.'),
      'width' => $this->t('Rokka.io service provided width.'),
      'format' => $this->t('Rokka.io service provided format.'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getIds() {
    $ids['uri']['type'] = 'string';
    $ids['uri']['alias'] = 'rm';

    return $ids;
  }

}
