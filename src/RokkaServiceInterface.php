<?php

declare(strict_types=1);

namespace Drupal\rokka;

use Rokka\Client\Image;
use Rokka\Client\User;

/**
 * Rokka service interface.
 */
interface RokkaServiceInterface {

  /**
   * Get the Rokka image client.
   *
   * @return \Rokka\Client\Image
   *   Client.
   */
  public function getRokkaImageClient(): Image;

  /**
   * Get the Rokka user client.
   *
   * @return \Rokka\Client\User
   *   User.
   */
  public function getRokkaUserClient(): User;

  /**
   * Get an image, given the URI.
   *
   * @param string $uri
   *   URI.
   *
   * @return \Drupal\rokka\Entity\RokkaMetadata[]
   *   Entities.
   */
  public function loadRokkaMetadataByUri(string $uri): array;

  /**
   * Counts the number of images that share the same Hash.
   *
   * @param string $hash
   *   Hash.
   *
   * @return int
   *   Count.
   */
  public function countImagesWithHash(string $hash): int;

  /**
   * Returns the organization name.
   *
   * @return mixed
   *   Organization name.
   */
  public function getRokkaOrganizationName();

}
