<?php

declare(strict_types=1);

namespace Drupal\rokka\StyleEffects;

use Drupal\rokka\ImageStyleHelper;
use Rokka\Client\Core\StackOperation;

/**
 * FocalPointCrop effect.
 */
class EffectFocalPointCrop implements ImageEffectInterface {

  /**
   * {@inheritdoc}
   */
  public static function buildRokkaStackOperation(array $data): array {
    $options = [
      'height' => ImageStyleHelper::operationNormalizeSize($data['height']),
      'width' => ImageStyleHelper::operationNormalizeSize($data['width']),
      'anchor' => 'auto',
    ];
    return [new StackOperation('crop', $options)];
  }

}
