<?php

declare(strict_types=1);

namespace Drupal\rokka\StyleEffects;

use Rokka\Client\Core\StackOperation;

/**
 * RokkaTextOverlay effect.
 */
class EffectRokkaImageEffectsTextOverlay implements ImageEffectInterface {

  /**
   * {@inheritdoc}
   */
  public static function buildRokkaStackOperation(array $data): array {
    $text_options = [
      'size' => $data['size'],
      'opacity' => $data['opacity'],
      'height' => $data['height'],
      'width' => $data['width'],
      'resize_to_box' => $data['resize_to_box'],
      'angle' => (int) $data['angle'],
      'text' => $data['text'],
      'font' => $data['font'],
      'anchor' => $data['anchor'],
      'color' => $data['color'],
      'align' => $data['align'],
    ];

    return [
      new StackOperation('text', $text_options),
    ];
  }

}
