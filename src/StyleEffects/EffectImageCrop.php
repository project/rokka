<?php

declare(strict_types=1);

namespace Drupal\rokka\StyleEffects;

/**
 * ImageCrop effect.
 */
class EffectImageCrop extends EffectRokkaCrop {

  /**
   * {@inheritdoc}
   */
  public static function buildRokkaStackOperation(array $data): array {
    $data = array_merge($data, [
      'background_color' => '#000000',
      'background_opacity' => 100,
    ]);

    return parent::buildRokkaStackOperation($data);
  }

}
