<?php

declare(strict_types=1);

namespace Drupal\rokka\StyleEffects;

use Rokka\Client\Core\StackOperation;

/**
 * ImageEffectsGaussianBlur effect.
 */
class EffectImageEffectsGaussianBlur implements ImageEffectInterface {

  /**
   * {@inheritdoc}
   */
  public static function buildRokkaStackOperation(array $data): array {
    $options = [
      'sigma' => $data['sigma'],
    ];
    return [new StackOperation('blur', $options)];
  }

}
