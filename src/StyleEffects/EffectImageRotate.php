<?php

declare(strict_types=1);

namespace Drupal\rokka\StyleEffects;

use Drupal\rokka\ImageStyleHelper;
use Rokka\Client\Core\StackOperation;

/**
 * ImageRotate effect.
 */
class EffectImageRotate implements ImageEffectInterface {

  /**
   * {@inheritdoc}
   */
  public static function buildRokkaStackOperation(array $data): array {

    $useTransparency = empty($data['bgcolor']);
    // If no background color has been defined, use white with 0 opacity.
    $backgroundColor = $useTransparency ? 'FFFFFF' : ImageStyleHelper::operationNormalizeColor($data['bgcolor']);

    $options = [
      'angle' => ImageStyleHelper::operationNormalizeAngle($data['degrees']),
      'background_opacity' => $useTransparency ? 0 : 100,
      'background_color' => $backgroundColor,
    ];

    return [new StackOperation('rotate', $options)];
  }

}
