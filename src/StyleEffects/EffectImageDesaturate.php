<?php

declare(strict_types=1);

namespace Drupal\rokka\StyleEffects;

use Rokka\Client\Core\StackOperation;

/**
 * ImageDesaturate effect.
 */
class EffectImageDesaturate implements ImageEffectInterface {

  /**
   * {@inheritdoc}
   */
  public static function buildRokkaStackOperation(array $data): array {

    return [new StackOperation('grayscale', [])];
  }

}
