<?php

declare(strict_types=1);

namespace Drupal\rokka\StyleEffects;

/**
 * Image effect interface.
 */
interface ImageEffectInterface {

  /**
   * Build Rokka Stack Operation.
   *
   * @param array $data
   *   Input data.
   *
   * @return \Rokka\Client\Core\StackOperation[]
   *   Stack operations.
   */
  public static function buildRokkaStackOperation(array $data): array;

}
