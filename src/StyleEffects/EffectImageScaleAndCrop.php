<?php

declare(strict_types=1);

namespace Drupal\rokka\StyleEffects;

use Drupal\rokka\ImageStyleHelper;
use Rokka\Client\Core\StackOperation;

/**
 * ImageScaleAndCrop effect.
 */
class EffectImageScaleAndCrop implements ImageEffectInterface {

  /**
   * {@inheritdoc}
   */
  public static function buildRokkaStackOperation(array $data): array {
    $options = [
      'height' => ImageStyleHelper::operationNormalizeSize($data['height']),
      'width' => ImageStyleHelper::operationNormalizeSize($data['width']),
    ];

    return [
      new StackOperation('resize', array_merge($options, ['mode' => 'fill'])),
      new StackOperation('crop', $options),
    ];
  }

}
