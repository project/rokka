<?php

declare(strict_types=1);

namespace Drupal\rokka\StyleEffects;

use Drupal\rokka\ImageStyleHelper;
use Rokka\Client\Core\StackOperation;

/**
 * ImageScale effect.
 */
class EffectImageScale extends EffectImageResize {

  /**
   * {@inheritdoc}
   */
  public static function buildRokkaStackOperation(array $data): array {
    $options = [
      'upscale' => boolval($data['upscale']),
      'height' => ImageStyleHelper::operationNormalizeSize($data['height']),
      'width' => ImageStyleHelper::operationNormalizeSize($data['width']),
    ];

    return [new StackOperation('resize', $options)];
  }

}
