<?php

declare(strict_types=1);

namespace Drupal\rokka\StyleEffects;

use Drupal\rokka\ImageStyleHelper;
use Rokka\Client\Core\StackOperation;

/**
 * RokkaBlur effect.
 */
class EffectRokkaBlur implements ImageEffectInterface {

  /**
   * {@inheritdoc}
   */
  public static function buildRokkaStackOperation(array $data): array {
    $options = [
      'radius' => ImageStyleHelper::operationNormalizeSize($data['radius']),
      'sigma' => ImageStyleHelper::operationNormalizeSize($data['sigma']),
    ];

    return [
      new StackOperation('blur', $options),
    ];
  }

}
