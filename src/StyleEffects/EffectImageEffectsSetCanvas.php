<?php

declare(strict_types=1);

namespace Drupal\rokka\StyleEffects;

use Drupal\rokka\ImageStyleHelper;
use Rokka\Client\Core\StackOperation;

/**
 * ImageEffectsSetCanvas effect.
 */
class EffectImageEffectsSetCanvas implements ImageEffectInterface {

  /**
   * {@inheritdoc}
   */
  public static function buildRokkaStackOperation(array $data): array {
    $options = [
      'mode' => 'foreground',
      'secondary_color' => substr(str_replace('#', '', $data['canvas_color']), 0, 6),
    ];

    // Width and height are optional.
    if (!empty($data['exact']['width'])) {
      $options['width'] = ImageStyleHelper::operationNormalizeSize($data['exact']['width']);
    }
    if (!empty($data['exact']['height'])) {
      $options['height'] = ImageStyleHelper::operationNormalizeSize($data['exact']['height']);
    }

    return [new StackOperation('composition', $options)];
  }

}
