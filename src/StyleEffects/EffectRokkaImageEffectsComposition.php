<?php

declare(strict_types=1);

namespace Drupal\rokka\StyleEffects;

use Rokka\Client\Core\StackOperation;

/**
 * RokkaComposition effect.
 */
class EffectRokkaImageEffectsComposition implements ImageEffectInterface {

  /**
   * {@inheritdoc}
   */
  public static function buildRokkaStackOperation(array $data): array {
    $composition_options = [
      'width' => $data['width'],
      'height' => $data['height'],
      'secondary_opacity' => $data['secondary_opacity'],
      'angle' => (int) $data['angle'],
      'mode' => $data['mode'],
      'resize_mode' => $data['resize_mode'],
      'anchor' => $data['anchor'],
      'resize_to_primary' => $data['resize_to_primary'],
      'secondary_color' => $data['secondary_color'],
      'secondary_image' => $data['secondary_image'],
    ];

    return [
      new StackOperation('composition', $composition_options),
    ];
  }

}
