<?php

declare(strict_types=1);

namespace Drupal\rokka\StyleEffects;

use Rokka\Client\Core\StackOperation;

/**
 * RokkaTrim effect.
 */
class EffectRokkaTrim implements ImageEffectInterface {

  /**
   * {@inheritdoc}
   */
  public static function buildRokkaStackOperation(array $data): array {
    $options = [
      'fuzzy' => static::normalizePercent($data['fuzzy']),
    ];
    return [new StackOperation('trim', $options)];
  }

  /**
   * Normalize percentage.
   *
   * @param mixed $value
   *   Percentage.
   *
   * @return mixed
   *   Percentage.
   */
  protected static function normalizePercent($value) {
    $value = $value ? $value : 0;
    return min(100, max(0, $value));
  }

}
