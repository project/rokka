<?php

declare(strict_types=1);

namespace Drupal\rokka;

use Drupal\Core\StreamWrapper\StreamWrapperInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\rokka\Entity\RokkaMetadata;
use Drupal\rokka\Entity\RokkaMetadataInterface;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Psr7\CachingStream;
use GuzzleHttp\Psr7\Stream;
use Rokka\Client\Core\SourceImage;

/**
 * The stream wrapper.
 *
 * phpcs:disable Drupal.NamingConventions.ValidFunctionName
 */
class RokkaStreamWrapper implements StreamWrapperInterface {

  use StringTranslationTrait;

  /**
   * The logger.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * The rokka service.
   *
   * @var \Drupal\rokka\RokkaServiceInterface
   */
  protected $rokkaService;

  /**
   * The file storage.
   *
   * @var \Drupal\file\FileStorageInterface
   */
  protected $fileStorage;

  /**
   * The supported modes.
   *
   * @var string[]
   */
  public static $supportedModes = ['w', 'r'];

  /**
   * The image client.
   *
   * @var \Rokka\Client\Image
   */
  protected static $imageClient;

  /**
   * The stream body.
   *
   * @var \Psr\Http\Message\StreamInterface
   */
  protected $body;

  /**
   * The URI.
   *
   * @var string
   */
  protected $uri;

  /**
   * The mode.
   *
   * @var string
   */
  protected $mode;

  /**
   * Construct a new stream wrapper.
   */
  public function __construct() {
    // Dependency injection will not work here, since stream wrappers
    // are not loaded the normal way: PHP creates them automatically
    // when certain file functions are called.
    $this->logger = \Drupal::service('rokka.logger');
    $this->rokkaService = \Drupal::service('rokka.service');
    $this->fileStorage = \Drupal::entityTypeManager()->getStorage('file');
    static::$imageClient = $this->rokkaService->getRokkaImageClient();
  }

  /**
   * {@inheritdoc}
   */
  public function getUri(): string {
    return $this->uri;
  }

  /**
   * {@inheritdoc}
   */
  public function setUri($uri): void {
    $this->uri = $uri;
  }

  /**
   * {@inheritdoc}
   */
  public function stream_close(): bool {
    if ($this->body) {
      $this->body->close();
      $this->body = NULL;
      return TRUE;
    }

    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function stream_open($path, $mode, $options, &$opened_path): bool {
    $this->uri = $path;

    // We don't care about the binary flag.
    $this->mode = rtrim($mode, 'bt');

    $exceptions = [];
    if (strpos($this->mode, '+')) {
      $exceptions[] = new \LogicException('The RokkaStreamWrapper does not support simultaneous reading and writing (mode: {' . $this->mode . '}).');
    }
    if (!in_array($this->mode, static::$supportedModes, TRUE)) {
      $exceptions[] = new \LogicException('Mode not supported: {' . $this->mode . '}. Use one "r", "w".', 400);
    }

    if (empty($exceptions)) {
      // This stream is Write-Only since the stream is not reversible for Read
      // and Write operations from the same filename: to read from a previously
      // written filename, the HASH must be provided.
      if ($this->mode === 'w') {
        $this->openWriteStream($exceptions);
      }

      if ($this->mode === 'r') {
        $this->openReadStream($exceptions);
      }
    }

    if (!empty($exceptions)) {
      return $this->triggerException($exceptions);
    }

    return TRUE;
  }

  /**
   * Initializes the stream wrapper for a write only stream.
   *
   * @param array $errors
   *   Any encountered errors to append to.
   *
   * @return bool
   *   Whether the stream was successfully opened.
   */
  protected function openWriteStream(array &$errors): ?bool {
    // We must check HERE if the underlying connection to Rokka is working fine
    // instead of returning FALSE during stream_flush() and stream_close() if
    // Rokka service is not available.
    // Reason: The PHP core, in the "_php_stream_copy_to_stream_ex()" function,
    // is not checking if the stream contents got successfully written after the
    // source and destination streams have been opened.
    try {
      // @todo Using listStack() invocation to check if Rokka is still alive,
      // but we must use a better API invocation here!
      self::$imageClient->listStacks(1);
      $this->body = new Stream(fopen('php://temp', 'r+'));
      return TRUE;
    }
    catch (\Exception $e) {
      $errors[] = $e;
      return $this->triggerException($errors);
    }
  }

  /**
   * Initializes the stream wrapper for a read only stream.
   *
   * @param array $errors
   *   Any encountered errors to append to.
   *
   * @return bool
   *   Whether the stream was successfully opened.
   */
  protected function openReadStream(array &$errors): bool {
    $meta = $this->doGetMetadataFromUri($this->uri);
    if (!$meta) {
      $errors[] = new LogicException('Unable to determine the Rokka.io HASH for the current URI.', 404);
      return $this->triggerException($errors);
    }

    try {
      // Load the binary data directly from the source image or the image
      // derivative.
      $file_url = $this->getExternalUrl();
      if (empty($file_url)) {
        // Drupal errors logged earlier, abort going further.
        return FALSE;
      }
      $sourceStream = fopen($file_url, 'r');
      $this->body = new Stream($sourceStream, 'rb');

      // Wrap the body in a caching entity body if seeking is allowed.
      if (!$this->body->isSeekable()) {
        $this->body = new CachingStream($this->body);
      }
    }
    catch (\Exception $e) {
      $errors[] = $e;
      return $this->triggerException($errors);
    }
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function stream_write($data): int {
    return $this->body->write($data);
  }

  /**
   * {@inheritdoc}
   */
  public function stream_eof(): bool {
    return $this->body->eof();
  }

  /**
   * {@inheritdoc}
   */
  public function stream_tell(): int {
    return $this->body->tell();
  }

  /**
   * {@inheritdoc}
   */
  public function stream_flush(): ?bool {
    if ('r' === $this->mode) {
      // Read only Streams can not be flushed, just return true.
      return TRUE;
    }
    $this->body->rewind();
    try {
      $imageCollection = static::$imageClient->uploadSourceImage(
        $this->body->getContents(),
        basename($this->uri)
      );

      if (1 !== $imageCollection->count()) {
        $exception = new \LogicException('RokkaStreamWrapper: No SourceImage data returned after invoking uploadSourceImage()!', 404);
        return $this->triggerException($exception);
      }

      /** @var \Rokka\Client\Core\SourceImage $image */
      $source_images = $imageCollection->getSourceImages();
      $image = reset($source_images);
      $image->size = $this->body->getSize();

      // Invoking Post-Save callback.
      $this->doPostSourceImageSaved($image);
    }
    catch (\Exception $e) {
      $this->body = NULL;
      return $this->triggerException($e);
    }

    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function stream_stat(): array {
    return [
      'size' => $this->body->getSize(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function sanitizeUri(string $uri) {
    return preg_replace('&styles/.*/rokka/&', '', $uri);
  }

  /**
   * {@inheritdoc}
   *
   * The Rokka.io service has no locking capability, so return TRUE.
   */
  public function stream_lock($operation): bool {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function stream_read($count) {
    if ($this->mode === 'r') {
      return $this->body->read($count);
    }
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function stream_seek($offset, $whence = SEEK_SET): bool {
    if ($this->body->isSeekable()) {
      $this->body->seek($offset, $whence);
      return TRUE;
    }
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function chmod($mode): bool {
    return TRUE;
  }

  /**
   * Helper function to prepare a url_stat result array.
   *
   * All files and folders will be returned with 0777 permission.
   *
   * @param string|array $result
   *   Data to add
   *   - Null or String for Folders
   *   - Array for Files with the following keyed values:
   *    - 'timestamp': the creation/modification timestamp
   *    - 'filesize': the file dimensions.
   *
   * @return array
   *   Returns the modified url_stat result
   */
  protected function formatUrlStat($result = NULL): array {
    static $statTemplate = [
      0 => 0,
      'dev' => 0,
      1 => 0,
      'ino' => 0,
      2 => 0,
      'mode' => 0,
      3 => 0,
      'nlink' => 0,
      4 => 0,
      'uid' => 0,
      5 => 0,
      'gid' => 0,
      6 => -1,
      'rdev' => -1,
      7 => 0,
      'size' => 0,
      8 => 0,
      'atime' => 0,
      9 => 0,
      'mtime' => 0,
      10 => 0,
      'ctime' => 0,
      11 => -1,
      'blksize' => -1,
      12 => -1,
      'blocks' => -1,
    ];
    $stat = $statTemplate;
    $type = gettype($result);
    // Determine what type of data is being cached.
    if ($type === 'NULL' || $type === 'string') {
      // Directory with 0777 access - see "man 2 stat".
      $stat['mode'] = $stat[2] = 0040777;
    }
    elseif ($type === 'array' && isset($result['timestamp'])) {
      // ListObjects or HeadObject result.
      $stat['mtime'] = $stat[9] = $stat['ctime'] = $stat[10] = $result['timestamp'];
      // $stat['atime'] = $stat[8] = $result['timestamp'];.
      $stat['size'] = $stat[7] = $result['filesize'];
      // Regular file with 0777 access - see "man 2 stat".
      $stat['mode'] = $stat[2] = 0100777;
    }
    return $stat;
  }

  /**
   * Returns the file extension of a given file name.
   *
   * @todo No longer relevant?
   * @todo This public method is not exposed on the interface. Either remove it
   *   or provide a dedicated interface.
   */
  public static function getMimeType($uri, $mapping = NULL) {
    if (!isset($mapping)) {
      // The default file map, defined in file.mimetypes.inc is quite big.
      // We only load it when necessary.
      include_once DRUPAL_ROOT . '/includes/file.mimetypes.inc';
      $mapping = file_mimetype_mapping();
    }

    $extension = '';
    $file_parts = explode('.', basename($uri));

    // Remove the first part: a full filename should not match an extension.
    array_shift($file_parts);

    // Iterate over the file parts, trying to find a match.
    // For my.awesome.image.jpeg, we try:
    // - jpeg
    // - image.jpeg, and
    // - awesome.image.jpeg.
    while ($additional_part = array_pop($file_parts)) {
      $extension = strtolower($additional_part . ($extension ? '.' . $extension : ''));
      if (isset($mapping['extensions'][$extension])) {
        return $mapping['mimetypes'][$mapping['extensions'][$extension]];
      }
    }
    return 'application/octet-stream';
  }

  /**
   * {@inheritdoc}
   */
  public static function getType(): int {
    return StreamWrapperInterface::NORMAL;
  }

  /**
   * Returns the portion of the URI to the right of the scheme.
   *
   * For example, in rokka://test.txt, the target is 'example/test.txt'.
   *
   * @param string|null $uri
   *   The URI.
   *
   * @return string
   *   The target.
   *
   * @todo This public method is not exposed on the interface. Either remove it
   *   or provide a dedicated interface.
   */
  public function getTarget(string $uri = NULL): string {
    if (!isset($uri)) {
      $uri = $this->uri;
    }

    $split_uri = explode('://', $uri);
    // Remove erroneous leading or trailing, forward-slashes and backslashes.
    // In the rokka:// scheme, there is never a leading slash on the target.
    return trim($split_uri[1], '\/');
  }

  /**
   * Implements LocalStream::getDirectoryPath().
   *
   * This method is not part of the interface, but exists on the LocalStream
   * base class which is widely used in core and contrib modules. We provide it
   * to maximize compatibility.
   *
   * In this case there is no local directory since the data is hosted on an
   * external service, so return an empty string.
   */
  public function getDirectoryPath() {
    return '';
  }

  /**
   * {@inheritdoc}
   */
  public function getExternalUrl(): string {

    $stack_name = 'dynamic/noop';

    if (strpos($this->uri, 'rokka://styles/') === 0) {
      $exploded_uri = explode('/', $this->uri);
      $stack_name = $exploded_uri[3];
    }

    $meta = $this->doGetMetadataFromUri($this->uri);

    if (!($meta instanceof RokkaMetadata)) {
      $this->logger->critical('Error getting getExternalUrl() for "{uri}": RokkaMetadata not found!', [
        'uri' => $this->uri,
      ]);

      // If no metadata is found an empty string is returned. This is required
      // by the StreamWrapperInterface. If NULL were returned
      // FileUrlGenerator::doGenerateString() would result in a type error.
      return '';
    }

    $defaultStyle = $stack_name;
    $name = NULL;

    $filename = pathinfo($meta->getUri(), PATHINFO_FILENAME);
    $name = RokkaService::cleanRokkaSeoFilename($filename);

    /** @var \Drupal\rokka\Entity\RokkaStack $stackEntity */
    $stackEntity = $this->rokkaService->loadStackByName($stack_name);
    $outputFormat = $meta->getFormat() ?? 'jpg';
    if ($stackEntity && $outputFormat !== 'png') {
      // Let the rokka stack alter the output image format,
      // except for PNG because else we lose transparencies.
      $outputFormat = $stackEntity->getOutputFormat() ?? 'jpg';
    }
    $externalUri = self::$imageClient->getSourceImageUri(
      $meta->getHash(),
      $defaultStyle,
      $outputFormat,
      $name
    );
    return (string) $externalUri;
  }

  /**
   * Get Metadata from URI.
   *
   * Callback function invoked by the underlying stream when the Rokka HASH is
   * needed instead of the standard URI (examples includes the deletion of an
   * image from Rokka.io or the uri_stat() function).
   *
   * @param string $uri
   *   URI.
   *
   * @return \Drupal\rokka\Entity\RokkaMetadata|null
   *   Metadata entity.
   */
  protected function doGetMetadataFromUri($uri): ?RokkaMetadata {
    $metadata = $this->rokkaService
      ->loadRokkaMetadataByUri($this->sanitizeUri($uri));
    if (!empty($metadata)) {
      return reset($metadata);
    }
    return NULL;
  }

  /**
   * Support for unlink().
   *
   * Override the unlink() function. Instead of directly deleting the underlying
   * Rokka image, we must check if the same HASH has been assigned to another
   * file: this can happen when the user uploads multiple time the same image.
   * For each uploaded image Drupal will assign it a different FID/URI, but
   * Rokka is referencing to the same HASH computed on the image contents.
   *
   * @param string $uri
   *   A string containing the uri to the resource to delete.
   *
   * @return bool
   *   TRUE if resource was successfully deleted.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \GuzzleHttp\Exception\GuzzleException
   *
   * @see http://php.net/manual/en/streamwrapper.unlink.php
   */
  public function unlink($uri): bool {
    // Derivatives are silently ignored.
    if (strpos($uri, 'rokka://styles/') === 0) {
      return TRUE;
    }

    $meta = $this->doGetMetadataFromUri($uri);
    if (!$meta) {
      return FALSE;
    }

    $hash = $meta->getHash();
    $sharedHashesCount = $this->rokkaService->countImagesWithHash($hash);

    // This should never occur when using Drupal's UI since the URI is assumed
    // to be unique, but we check this to avoid issues with custom code reusing
    // the metadata.
    $filesUsingMetadata = $this->fileStorage
      ->getQuery()
      ->accessCheck(FALSE)
      ->condition('uri', $meta->getUri())
      ->count()
      ->execute();

    if ($sharedHashesCount > 1 || $filesUsingMetadata > 1) {
      // If the same hash is used elsewhere for another file,
      // remove the Drupal file entity, but not the metadata entity nor
      // the actual file itself.
      $this->doPostSourceImageDeleted($meta);
      $this->logger->debug('Image file "{uri}" deleted, but kept in Rokka since HASH "{hash}" is not unique on Drupal.', [
        'uri' => $uri,
        'hash' => $meta->getHash(),
      ]);

      return TRUE;
    }

    $this->logger->debug('Deleting image file "{uri}".', [
      'uri' => $uri,
      'hash' => $meta->getHash(),
    ]);

    $meta = $this->doGetMetadataFromUri($uri);

    if (!$meta || empty($meta->getHash())) {
      $exception = new \LogicException('Unable to determine the Rokka.io HASH for the current URI.', 404);
      return $this->triggerException($exception);
    }
    try {
      $delete_image = self::$imageClient->deleteSourceImage($meta->getHash());
      if ($delete_image) {
        $this->doPostSourceImageDeleted($meta);
      }
      return $delete_image;
    }
    catch (\Exception $e) {
      return $this->triggerException($e, STREAM_URL_STAT_QUIET);
    }
  }

  /**
   * Post source image deleted.
   *
   * Callback function invoked after the underlying Stream has been unlinked and
   * the corresponding image deleted on Rokka.io
   * The callback receives the $hash used to remove the image.
   *
   * @param \Drupal\rokka\Entity\RokkaMetadataInterface $metadata_entity
   *   Rokka Metadata entity.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function doPostSourceImageDeleted(RokkaMetadataInterface $metadata_entity): void {
    $metadata_entity->delete();
  }

  /**
   * Support for stat().
   *
   * This important function goes back to the Unix way of doing things.
   * In this example almost the entire stat array is irrelevant, but the
   * mode is very important. It tells PHP whether we have a file or a
   * directory and what the permissions are. All that is packed up in a
   * bitmask. This is not normal PHP fodder.
   *
   * @param string $uri
   *   A string containing the URI to get information about.
   * @param int $flags
   *   A bit mask of STREAM_URL_STAT_LINK and STREAM_URL_STAT_QUIET.
   *
   * @return array|bool
   *   An array with file status, or FALSE in case of an error - see fstat()
   *   for a description of this array.
   *
   *   Use the formatUrlStat() function as an helper for return values.
   *
   * @see http://php.net/manual/en/streamwrapper.url-stat.php
   */
  public function url_stat($uri, $flags) {
    if ($this->is_dir($uri)) {
      return $this->formatUrlStat();
    }

    $meta = $this->doGetMetadataFromUri($uri);
    if ($meta) {
      $data = [
        'timestamp' => $meta->getCreatedTime(),
        'filesize' => $meta->getFilesize(),
      ];

      return $this->formatUrlStat($data);
    }

    return FALSE;
  }

  /**
   * Is a directory.
   *
   * @param string $uri
   *   URI.
   *
   * @return bool
   *   Is a directory.
   */
  protected function is_dir($uri): bool {
    $split_uri = explode('://', $uri);
    $target = $split_uri[1];

    // Check if it's the root directory.
    if (empty($target)) {
      return TRUE;
    }

    // Check if URI has a file ending. If not, it's a directory.
    // This fixes compatibility with media library upload.
    $info = pathinfo($target);
    return empty($info['extension']);
  }

  /**
   * {@inheritdoc}
   */
  public function realpath(): bool {
    return FALSE;
  }

  /**
   * Gets the name of the directory from a given path.
   *
   * A trailing "/" is always appended to mark the resource as a Directory.
   *
   * @param string $uri
   *   A URI.
   *
   * @return string
   *   A string containing the directory name.
   *
   * @see drupal_dirname()
   */
  public function dirname($uri = NULL): string {
    [$scheme, $target] = explode('://', $uri, 2);
    $target = $this->getTarget($uri);
    if (strpos($target, '/')) {
      // If we matched a directory here, let's append '/' in the end.
      $dirname = preg_replace('@/[^/]*$@', '', $target) . '/';
    }
    else {
      $dirname = '';
    }
    return $scheme . '://' . $dirname;
  }

  /**
   * Rokka has no support for mkdir(), thus we 'virtually' create them.
   *
   * @param string $uri
   *   A string containing the URI to the directory to create.
   * @param int $mode
   *   Permission flags - see mkdir().
   * @param int $options
   *   A bit mask of STREAM_REPORT_ERRORS and STREAM_MKDIR_RECURSIVE.
   *
   * @return bool
   *   TRUE if directory was successfully created.
   *
   * @see http://php.net/manual/en/streamwrapper.mkdir.php
   */
  public function mkdir($uri, $mode, $options): bool {
    // Some Drupal plugins call mkdir with a trailing slash. We mustn't store
    // that slash in the cache.
    $uri = rtrim($uri, '/');

    // If the STREAM_MKDIR_RECURSIVE option was specified, also create all the
    // ancestor folders of this uri, except for the root directory.
    $parent_dir = \Drupal::service('file_system')->dirname($uri);
    if (($options & STREAM_MKDIR_RECURSIVE) && \Drupal::service('stream_wrapper_manager')->getTarget($parent_dir) != '') {
      return $this->mkdir($parent_dir, $mode, $options);
    }
    return TRUE;
  }

  /**
   * Rokka.io has no support for rmdir().
   *
   * @param string $uri
   *   A string containing the URI to the directory to delete.
   * @param int $options
   *   A bit mask of STREAM_REPORT_ERRORS.
   *
   * @return bool
   *   TRUE if the directory was successfully deleted.
   *
   *   Always return FALSE. (not supported)
   *
   * @see http://php.net/manual/en/streamwrapper.rmdir.php
   */
  public function rmdir($uri, $options): bool {
    return FALSE;
  }

  /**
   * Rokka.io has no support for rename().
   *
   * @param string $from_uri
   *   The uri to the file to rename.
   * @param string $to_uri
   *   The new uri for file.
   *
   * @return bool
   *   Always returns FALSE. (not supported)
   *
   * @see http://php.net/manual/en/streamwrapper.rename.php
   */
  public function rename($from_uri, $to_uri): bool {
    return FALSE;
  }

  /**
   * Rokka.io has no support for opendir().
   *
   * @param string $uri
   *   A string containing the URI to the directory to open.
   * @param int $options
   *   Whether or not to enforce safe_mode (0x04).
   *
   * @return bool
   *   TRUE on success.
   *
   * @see http://php.net/manual/en/streamwrapper.dir-opendir.php
   */
  public function dir_opendir($uri, $options): bool {
    return $this->is_dir($uri);
  }

  /**
   * Rokka.io has no support for readdir().
   *
   * @return string|bool
   *   Always returns FALSE (not supported).
   *
   * @see http://php.net/manual/en/streamwrapper.dir-readdir.php
   */
  public function dir_readdir() {
    return FALSE;
  }

  /**
   * Rokka.io has not support for rewinddir().
   *
   * @return bool
   *   TRUE on success.
   *
   *   Always returns FALSE. (not supported)
   *
   * @see http://php.net/manual/en/streamwrapper.dir-rewinddir.php
   */
  public function dir_rewinddir(): bool {
    return FALSE;
  }

  /**
   * Rokka.io has no support for closedir().
   *
   * @return bool
   *   TRUE on success.
   *
   *   Always returns TRUE. (not supported)
   *
   * @see http://php.net/manual/en/streamwrapper.dir-closedir.php
   */
  public function dir_closedir(): bool {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   *
   * Always returns FALSE.
   *
   * @see stream_select()
   * @see http://php.net/manual/streamwrapper.stream-cast.php
   */
  public function stream_cast($cast_as) {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   *
   * This wrapper does not support touch(), chmod(), chown(), or chgrp().
   *
   * Manual recommends return FALSE for not implemented options, but Drupal
   * require TRUE in some cases like chmod for avoid watchdog erros.
   *
   * Returns FALSE if the option is not included in bypassed_options array
   * otherwise, TRUE.
   *
   * @see http://php.net/manual/en/streamwrapper.stream-metadata.php
   * @see \Drupal\Core\File\FileSystem::chmod()
   */
  public function stream_metadata($uri, $option, $value): bool {
    $bypassed_options = [STREAM_META_ACCESS];
    return in_array($option, $bypassed_options, TRUE);
  }

  /**
   * {@inheritdoc}
   *
   * Since Windows systems do not allow it and it is not needed for most use
   * cases anyway, this method is not supported on local files and will trigger
   * an error and return false. If needed, custom subclasses can provide
   * OS-specific implementations for advanced use cases.
   */
  public function stream_set_option($option, $arg1, $arg2) {
    trigger_error('stream_set_option() not supported for Rokka stream wrappers', E_USER_WARNING);
    return FALSE;
  }

  /**
   * {@inheritdoc}
   *
   * This wrapper does not support stream_truncate.
   *
   * Always returns FALSE.
   *
   * @see http://php.net/manual/en/streamwrapper.stream-truncate.php
   */
  public function stream_truncate($new_size): bool {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->t('Rokka');
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->t('Rokka Storage Service.');
  }

  /**
   * Return the local filesystem path.
   *
   * @param string|null $uri
   *   URI.
   *
   * @return string
   *   The local path.
   */
  protected function getLocalPath($uri = NULL): string {
    if (!isset($uri)) {
      $uri = $this->uri;
    }

    $path = str_replace('rokka://', '', $uri);
    $path = trim($path, '/');
    return $path;
  }

  /**
   * Post source image saved.
   *
   * Callback function invoked after the underlying Stream has been flushed to
   * Rokka.io, the callback receives the SourceImage returned by the
   * $client->uploadSourceimage() invocation.
   *
   * @param \Rokka\Client\Core\SourceImage $sourceImage
   *   Source Image.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   *
   * @see RokkaStreamWrapper::stream_flush()
   */
  protected function doPostSourceImageSaved(SourceImage $sourceImage): void {
    // At this point the image has been uploaded to Rokka.io for the
    // "rokka://URI". We use the rokka_metadata table to store the values
    // returned by Rokka such as: hash, filesize, etc. First check if the URI is
    // already tracked (i.e. the file has been overwritten).
    $metadata_entity = $this->doGetMetadataFromUri($this->uri);
    if ($metadata_entity) {

      // If the two images are the same we're done, just return true.
      if ($metadata_entity->getHash() === $sourceImage->hash) {
        $this->logger->debug('Image re-uploaded to Rokka for "{uri}": "{hash}" (hash did not change)', [
          'uri' => $this->uri,
          'hash' => $sourceImage->hash,
        ]);
        return;
      }

      $this->logger->debug('Image replaced on Rokka for "{uri}": "{hash}" (old hash: "{oldHash}")', [
        'uri' => $this->uri,
        'oldHash' => $metadata_entity->getHash(),
        'hash' => $sourceImage->hash,
      ]);

      // Update the RokkaMetadata with the new data of the uploaded image.
      $metadata_entity->hash = $sourceImage->hash;
      $metadata_entity->binary_hash = $sourceImage->binaryHash;
      $metadata_entity->created = $sourceImage->created->getTimestamp();
      $metadata_entity->filesize = $sourceImage->size;
      $metadata_entity->setHeight(abs($sourceImage->height));
      $metadata_entity->setWidth(abs($sourceImage->width));
      $metadata_entity->setFormat($sourceImage->format);
    }
    else {
      $this->logger->debug('New Image uploaded to Rokka for "{uri}": "{hash}"', [
        'uri' => $this->uri,
        'hash' => $sourceImage->hash,
      ]);

      $metadata_entity = RokkaMetadata::create([
        'uri' => $this->uri,
        'hash' => $sourceImage->hash,
        'binary_hash' => $sourceImage->binaryHash,
        'filesize' => $sourceImage->size,
        'created' => $sourceImage->created->getTimestamp(),
        'height' => abs($sourceImage->height),
        'width' => abs($sourceImage->width),
        'format' => $sourceImage->format,
      ]);
    }

    $metadata_entity->save();
  }

  /**
   * Trigger one or more errors.
   *
   * Override the default exception handling, logging errors to Drupal messages
   * and Watchdog.
   *
   * @param \Exception|\Exception[] $exceptions
   *   Exceptions.
   * @param mixed $flags
   *   If set to STREAM_URL_STAT_QUIET, then no error or
   *   exception occurs.
   *
   * @return bool
   *   Always returns false?!
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  protected function triggerException($exceptions, $flags = NULL): bool {
    $exceptions = is_array($exceptions) ? $exceptions : [$exceptions];

    /** @var \Exception $exception */
    foreach ($exceptions as $exception) {
      // If we got a GuzzleException here, means that something happened during
      // the data transfer. We throw the exception to Drupal.
      if ($exception instanceof GuzzleException) {
        \Drupal::messenger()->addError($this->t('An error occurred while communicating with the Rokka.io server!'));
      }

      $this->logger->critical(
        'Exception caught: {exceptionCode} "{exceptionMessage}". In {file} at line {line}.',
        [
          'exceptionCode' => $exception->getCode(),
          'exceptionMessage' => $exception->getMessage(),
          'file' => $exception->getFile(),
          'line' => $exception->getLine(),
        ]
      );
    }

    if (!($flags & STREAM_URL_STAT_QUIET)) {
      throw $exception;
    }

    if ($flags & STREAM_URL_STAT_QUIET) {
      // This is triggered with things like file_exists()
      // FIXME: Likely error here.
      if ($flags & STREAM_URL_STAT_LINK) {
        // This is triggered for things like is_link()
        // return $this->formatUrlStat(false);
      }
      return FALSE;
    }

    $exceptions = is_array($exceptions) ? $exceptions : [$exceptions];
    $messages = [];
    /** @var \Exception $exception */
    foreach ($exceptions as $exception) {
      $messages[] = $exception->getMessage();
    }

    trigger_error(implode("\n", $messages), E_USER_WARNING);
    return FALSE;
  }

}
