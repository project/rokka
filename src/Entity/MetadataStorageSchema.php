<?php

declare(strict_types=1);

namespace Drupal\rokka\Entity;

use Drupal\Core\Entity\Sql\SqlContentEntityStorageSchema;
use Drupal\Core\Field\FieldStorageDefinitionInterface;

/**
 * Metadata storage schema.
 */
class MetadataStorageSchema extends SqlContentEntityStorageSchema {

  /**
   * {@inheritdoc}
   */
  protected function getSharedTableFieldSchema(FieldStorageDefinitionInterface $storage_definition, $table_name, array $column_mapping): array {
    $schema = parent::getSharedTableFieldSchema($storage_definition, $table_name, $column_mapping);
    $field_name = $storage_definition->getName();

    if (in_array($field_name, ['binary_hash', 'uri', 'hash'], TRUE)) {
      $this->addSharedTableFieldIndex($storage_definition, $schema, TRUE);
    }

    return $schema;
  }

}
