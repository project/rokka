<?php

declare(strict_types=1);

namespace Drupal\rokka\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\image\Entity\ImageStyle;
use Drupal\rokka\StackHelper;
use Rokka\Client\Core\Stack as RemoteStack;

/**
 * Defines the Rokka stack entity.
 *
 * @ConfigEntityType(
 *   id = "rokka_stack",
 *   label = @Translation("Rokka stack"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\rokka\Entity\Controller\RokkaStackListBuilder",
 *     "form" = {
 *       "add" = "Drupal\rokka\Form\RokkaStackForm",
 *       "edit" = "Drupal\rokka\Form\RokkaStackForm",
 *       "delete" = "Drupal\rokka\Form\RokkaStackDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "rokka_stack",
 *   admin_permission = "administer rokka",
 *   entity_keys = {
 *     "id" = "id",
 *     "uuid" = "uuid",
 *   },
 *   links = {
 *     "delete-form" = "/admin/structure/rokka_stack/{rokka_stack}/delete",
 *     "collection" = "/admin/structure/rokka_stacks",
 *   },
 *   config_export = {
 *     "stackOptions",
 *     "id",
 *     "outputFormat"
 *   }
 * )
 */
class RokkaStack extends ConfigEntityBase implements RokkaStackInterface {

  use StringTranslationTrait;

  /**
   * The Rokka stack name.
   *
   * @var string
   */
  protected $id;

  /**
   * The Rokka stack $organization.
   *
   * @var string
   */
  protected $organization;

  /**
   * The Rokka output format.
   *
   * @var string
   */
  protected $outputFormat;

  /**
   * The Rokka stack options.
   *
   * @var array
   */
  protected $stackOptions;

  /**
   * The Rokka stack uuid.
   *
   * @var string
   */
  protected $uuid;

  /**
   * The label of this stack.
   *
   * @var string
   */
  protected $label;

  /**
   * Rokka Service.
   *
   * @var \Drupal\rokka\RokkaServiceInterface
   */
  protected $rokkaService;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $values, $entity_type) {
    parent::__construct($values, $entity_type);
    $this->rokkaService = \Drupal::service('rokka.service');
  }

  /**
   * {@inheritdoc}
   */
  public function getStackOptions(): array {
    return $this->stackOptions;
  }

  /**
   * {@inheritdoc}
   */
  public function setStackOptions(array $options): void {
    $this->stackOptions = $options;
  }

  /**
   * {@inheritdoc}
   */
  public function getOutputFormat(): string {
    return $this->outputFormat;
  }

  /**
   * {@inheritdoc}
   */
  public function setOutputFormat($outputFormat = NULL): void {
    $this->outputFormat = $outputFormat;
  }

  /**
   * {@inheritdoc}
   */
  public function postSave(EntityStorageInterface $storage, $update = TRUE): void {
    parent::postSave($storage, $update);

    // This lookup is not definitely guaranteed but by convention we keep
    // stack and style ID the same.
    /** @var \Drupal\image\Entity\ImageStyle $imageStyle */
    $imageStyle = \Drupal::entityTypeManager()
      ->getStorage('image_style')
      ->load($this->id());

    if (!$imageStyle) {
      \Drupal::logger('rokka')->error(
        'Missing image style @name',
        ['@name' => $this->id()]
      );
      return;
    }

    $stack = new RemoteStack(
      $this->rokkaService->getRokkaOrganizationName(),
      $this->id(),
      StackHelper::getEffectStack($imageStyle),
      $this->normalizeOptions()
    );
    try {
      $this->rokkaService
        ->getRokkaImageClient()
        ->saveStack($stack, ['overwrite' => TRUE]);
    }
    catch (\Exception $e) {
      \Drupal::logger('rokka')->error(
        'Cannot save remote Rokka stack @name',
        ['@name' => $imageStyle->label() ?? $this->id()]
      );
      \Drupal::messenger()->addWarning(
        $this->t(
          'Cannot save remote Rokka stack @name',
        ['@name' => $this->label() ?? $this->id()]
        )
      );
      return;
    }

    // Flush the image style cache.
    $imageStyle->flush();
  }

  /**
   * Derive inherited and specific stack options for Rokka.
   *
   * @return array
   *   Stack options and output format.
   */
  protected function normalizeOptions(): array {
    $defaultConfig = \Drupal::config('rokka.settings');
    $storedOptions = $this->stackOptions;

    $stackOptions = [];

    if (isset($storedOptions['jpg_quality']) && $storedOptions['jpg_quality'] > 0) {
      $stackOptions['jpg.quality'] = $storedOptions['jpg_quality'];
    }
    elseif ($defaultConfig->get('jpg_quality') > 0) {
      $stackOptions['jpg.quality'] = $defaultConfig->get('jpg_quality');
    }

    if (isset($storedOptions['webp_quality']) && $storedOptions['webp_quality'] > 0) {
      $stackOptions['webp.quality'] = $storedOptions['webp_quality'];
    }
    elseif ($defaultConfig->get('webp_quality') > 0) {
      $stackOptions['webp.quality'] = $defaultConfig->get('webp_quality');
    }

    $stackOptions['autoformat'] = $defaultConfig->get('autoformat');
    if (isset($storedOptions['autoformat']) && $storedOptions['autoformat'] !== NULL) {
      $stackOptions['autoformat'] = $storedOptions['autoformat'];
    }

    return $stackOptions;
  }

  /**
   * {@inheritdoc}
   */
  public function delete(): void {
    parent::delete();
    if (!$this->isNew()) {
      try {
        $this->rokkaService->getRokkaImageClient()->deleteStack($this->id());
      }
      catch (\Exception $e) {
        // If the Stack is not found, that means that it is already deleted.
        if ($e->getCode() !== 404) {
          \Drupal::logger('rokka')->error(
            'Exception while deleting rokka stack "@name". Error: "@error"',
            [
              '@name' => $this->id(),
              '@error' => $e->getMessage(),
            ]
          );
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function calculateDependencies() {
    parent::calculateDependencies();

    $style = ImageStyle::load($this->id());
    if ($style) {
      $this->addDependency('config', $style->getConfigDependencyName());
    }

    return $this;
  }

}
