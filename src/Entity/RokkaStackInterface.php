<?php

declare(strict_types=1);

namespace Drupal\rokka\Entity;

/**
 * Interface for the Rokka stack entity.
 */
interface RokkaStackInterface {

  /**
   * Get stack options.
   *
   * @return array
   *   The options.
   */
  public function getStackOptions(): array;

  /**
   * Set Stack options.
   *
   * @param array $options
   *   The options.
   */
  public function setStackOptions(array $options): void;

  /**
   * Get output format.
   *
   * @return string
   *   Format.
   */
  public function getOutputFormat(): string;

  /**
   * Set output format.
   *
   * @param string $outputFormat
   *   Output format.
   */
  public function setOutputFormat($outputFormat = NULL): void;

}
