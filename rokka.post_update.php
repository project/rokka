<?php

/**
 * @file
 * Post update functions for the Rokka module.
 */

declare(strict_types=1);

use Drupal\Core\Config\Entity\ConfigEntityUpdater;

/**
 * Add missing dependency on image style to each Rokka stack.
 */
function rokka_post_update_add_image_style_dependency(array &$sandbox = NULL) {
  /** @var \Drupal\Core\Config\Entity\ConfigEntityUpdater $config_entity_updater */
  $config_entity_updater = \Drupal::classResolver(ConfigEntityUpdater::class);
  $config_entity_updater->update($sandbox, 'rokka_stack', function () {
    return TRUE;
  });
}
