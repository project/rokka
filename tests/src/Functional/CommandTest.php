<?php

declare(strict_types=1);

namespace Drupal\Tests\rokka\Functional;

use Drupal\Tests\BrowserTestBase;
use Drush\TestTraits\DrushTestTrait;

/**
 * Tests the drush command instantiation.
 *
 * @group rokka
 */
class CommandTest extends BrowserTestBase {

  use DrushTestTrait;

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['rokka', 'image'];

  /**
   * Test service availability.
   */
  public function testLoad(): void {
    // No styles exist, so no action to perform.
    $this->drush('rokka:migrate-imagestyles');
    $this->assertStringContainsString('Image styles migration completed.', $this->getOutput());
  }

}
