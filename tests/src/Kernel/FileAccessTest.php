<?php

declare(strict_types=1);

namespace Drupal\Tests\file\Kernel;

use Drupal\file\Entity\File;
use Drupal\file\FileInterface;
use Drupal\KernelTests\KernelTestBase;
use Drupal\Tests\user\Traits\UserCreationTrait;

/**
 * Tests access to file entities using the Rokka stream wrapper.
 *
 * @group rokka
 */
class FileAccessTest extends KernelTestBase {

  use UserCreationTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'file',
    'rokka',
    'system',
    'user',
  ];

  /**
   * Test users.
   *
   * @var \Drupal\Core\Session\AccountInterface[]|null
   */
  protected ?array $users;

  /**
   * A test file.
   *
   * @var \Drupal\file\FileInterface|null
   */
  protected ?FileInterface $file;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installSchema('system', ['sequences']);
    $this->installEntitySchema('user');
    $this->installEntitySchema('file');
    $this->installEntitySchema('rokka_metadata');
    $this->installSchema('file', ['file_usage']);
    $this->installConfig('user');

    // Create a test user that has no access to content.
    $this->users['no_access'] = $this->createUser([], NULL, FALSE, ['uid' => 2]);

    // Create a test user that can access content.
    $this->users['access'] = $this->createUser(['access content'], NULL, FALSE, ['uid' => 3]);

    // Create a test file using the rokka:// stream wrapper.
    $this->file = File::create([
      'uid' => 1,
      'filename' => 'test.jpg',
      'uri' => 'rokka://test.jpg',
      'status' => FILE_STATUS_PERMANENT,
    ]);
    $this->file->save();
  }

  /**
   * Tests that users can view and download files using our stream wrapper.
   */
  public function testFileAccess() {
    // Since the Rokka images are public, all users should be able to download.
    $this->assertTrue($this->file->access('download', $this->users['no_access']));
    $this->assertTrue($this->file->access('download', $this->users['access']));

    // Only users with the 'access content' permission are able to view the file
    // entities.
    $this->assertFalse($this->file->access('view', $this->users['no_access']));
    $this->assertTrue($this->file->access('view', $this->users['access']));
  }

}
