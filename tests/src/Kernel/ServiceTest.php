<?php

declare(strict_types=1);

namespace Drupal\Tests\rokka\Kernel;

use Drupal\KernelTests\KernelTestBase;
use Drupal\rokka\Entity\RokkaMetadata;
use Drupal\rokka\RokkaService;
use Rokka\Client\Image;

/**
 * Test metadata service handling.
 *
 * @group rokka
 */
class ServiceTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['rokka', 'user', 'field'];

  /**
   * Rokka service.
   *
   * @var \Drupal\rokka\RokkaService
   */
  protected $rokka;

  /**
   * Metadata.
   *
   * @var \Drupal\rokka\Entity\RokkaMetadata
   */
  protected $exampleMetadata;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installEntitySchema('rokka_metadata');
    $this->installEntitySchema('rokka_stack');

    $this->rokka = $this->container->get('rokka.service');

    /** @var \Drupal\rokka\Entity\RokkaMetadata $metadata */
    $this->exampleMetadata = RokkaMetadata::create(
      [
        'hash' => 'h111111111111111111111111111111111111111',
        'binary_hash' => 'b111111111111111111111111111111111111111',
        'uri' => 'rokka://example.jpg',
        'format' => 'jpg',
      ]
    );
    $this->exampleMetadata->save();
  }

  /**
   * Test the service.
   */
  public function testServiceBase(): void {
    $this->assertInstanceOf(RokkaService::class, $this->rokka);
    $this->assertInstanceOf(Image::class, $this->rokka->getRokkaImageClient());
  }

  /**
   * Test the metadata.
   */
  public function testMetadata(): void {
    $matchingMetadata = $this->rokka->loadRokkaMetadataByUri($this->exampleMetadata->get('uri')->value);
    self::assertEquals(
      $this->exampleMetadata->getHash(),
      reset($matchingMetadata)->getHash()
    );

    self::assertEquals(
      1,
      $this->rokka->countImagesWithHash($this->exampleMetadata->getHash())
    );
  }

}
