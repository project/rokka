<?php

declare(strict_types=1);

namespace Drupal\Tests\rokka\Kernel;

use Drupal\image\Entity\ImageStyle;
use Drupal\KernelTests\KernelTestBase;
use Drupal\rokka\Entity\RokkaStack;

/**
 * Image style form test.
 *
 * @group rokka
 */
class ImageStyleFormTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['rokka', 'user', 'field', 'image', 'file'];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->installEntitySchema('rokka_stack');
    $this->installConfig('rokka');
    $this->installEntitySchema('file');
  }

  /**
   * Test the service.
   */
  public function testPresave(): void {
    $style = ImageStyle::create(['name' => 'my_style']);
    $style->set(
      'rokka_stack_options',
      [
        'autoformat' => 'none',
        'output_format' => 'jpg',
        'jpg_quality' => '',
        'webp_quality' => '',
      ]
    );
    $style->save();

    $stack = RokkaStack::load('my_style');
    self::assertInstanceOf(RokkaStack::class, $stack);
    $options = $stack->getStackOptions();
    self::assertEquals(NULL, $options['autoformat']);
    self::assertEquals(NULL, $options['jpg_quality']);
    self::assertEquals(NULL, $options['webp_quality']);

    $style->set(
      'rokka_stack_options',
      [
        'autoformat' => 'true',
        'output_format' => 'jpg',
        'jpg_quality' => 12,
        'webp_quality' => 100,
      ]
    );
    $style->save();

    $stack = RokkaStack::load('my_style');
    $options = $stack->getStackOptions();
    self::assertEquals(12, $options['jpg_quality']);
    self::assertEquals(100, $options['webp_quality']);
    self::assertEquals(TRUE, $options['autoformat']);

    $style->set(
      'rokka_stack_options',
      [
        'autoformat' => 'false',
        'output_format' => 'jpg',
        'jpg_quality' => 0,
        'webp_quality' => 0,
      ]
    );
    $style->save();

    $stack = RokkaStack::load('my_style');
    $options = $stack->getStackOptions();
    self::assertEquals(0, $options['jpg_quality']);
    self::assertEquals(0, $options['webp_quality']);
    self::assertEquals(FALSE, $options['autoformat']);
  }

}
