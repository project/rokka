INTRODUCTION
------------

This module integrates [Rokka.io](https://rokka.io) with Drupal: after setting
up your credentials the module allows to:

 - Automatically upload images from fields to Rokka by using the `rokka://`
stream wrapper
 - Synchronize Drupal's Image Styles to Rokka's ImageStacks
 - Display images from Rokka service


REQUIREMENTS
------------

- The requirements listed in composer.json
- A Rokka API key.

INSTALLATION
------------

Enable the Drupal module as usual and either use the UI or set the credentials
via configuration.

### Example configuration in settings.php

```php
$config['rokka.settings']['organization_name'] = 'your_org';
$config['rokka.settings']['api_key'] = 'your_key';
```

CONFIGURATION
------------

Apart from installing the module you will likely want to configure the
field storage of a particular field to use Rokka, to store the data in it.

You can also enable Rokka as an upload scheme in the WYSIWYG editor by
enabling it in the editor profile configuration.

### Setting a default image as a Rokka image

If you want a Rokka image to be the default image in your previews (helpful
for debugging and testing, for instance), set it as follows:

```sh
drush cset image.settings preview_image 'rokka://your_default_image.jpg'
```

### Using Focal Point

The Rokka module fully supports setting the focal point for derivates via the
`focal_point` module. Enable it as usual and use the image styles as desired.

### Using PDF Thumbnails for Media Documents
The rokka module supports PDF thumbnails. First, change the StreamWrapper
scheme on the media bundle file field to rokka. Then enable PDF thumbnails for
the desired media bundle, see /admin/config/media/rokka/pdf. After enabling PDF
thumbnails for documents, regenerate thumbnails using the batch mode, see
/admin/config/media/rokka/regenerate.

### HLS video streaming

With the field formatter `Rokka HLS Video` you can make use of an .m3u stream
with the player of your choice.

It is *highly recommended* that you optimize `rokka-video-hls.html.twig` for
your needs and replace the CDN library embedded on the instance itself with a
library definition in your theme.
